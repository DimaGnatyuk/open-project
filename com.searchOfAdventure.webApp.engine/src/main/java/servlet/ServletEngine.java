package servlet;

import engine.Engine;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Admin on 28.02.2016.
 */
@WebServlet("/engine")
public class ServletEngine extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        super.doPost(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

            /*if (request.getParameter("status")!=null)
            {
                EngineSession.auntification(request,"");

            }else {
                HttpSession session = request.getSession();
                out.println("id - " + session.getAttribute("id"));
                out.println("u_id - " + session.getAttribute("u_id"));
            }*/
        String api = getParameters("api", request)/*"1.0b"*/;
        String menu = getParameters("menu", request);
        String option = getParameters("option", request);
        String action = getParameters("action", request);
        String data = getParameters("data", request);

        //out.print(request.getParameter("data").toString());
//            api = "1.0b";
//            meny = "New";
//            option = "User";
        //action = "Album";
        //data = "";
            /**//**//**//*"data"*/

        System.out.println("logs: ("+api+" "+menu+" "+option+" "+action+" "+data+") end log...");
        Engine engine = new Engine(request, api, menu, option, action, data);
        out.println(engine.getResponce());
    }

    String getParameters(String ket, HttpServletRequest request) {
        String text = "";
        String tmp = request.getParameter(ket);
        if(!((tmp == null) || (tmp.trim().length() == 0 ))){
            text = tmp;
        }
        return text;
    }
}
