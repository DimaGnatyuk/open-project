package servlet;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import component.SystemComponent;
import dabaBase.Impl.DAO.Entity.UsersEntity;
import dabaBase.Service.Factory;
import engine.apiV1_0b.meny.optionNew.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Admin on 28.02.2016.
 */
public class EngineSession {
    public static final String MENY = "System";
    private static HttpSession session;

    public static void auntification (HttpServletRequest request,String data)
    {
        JsonParser parser = new JsonParser();
        data = "{"
                + "Email: \"dim_mon@ukr.net\","
                + "Password: \"199717\""
                +"}";
        JsonObject mainObject = parser.parse(data).getAsJsonObject();


        String email = mainObject.get("Email").getAsString();
        String password = mainObject.get("Password").getAsString();

        UsersEntity usersEntity = Factory.getInstance().getUserDAO().getUserByEmailAndPassword(email,password);

        session = request.getSession();
        session.setAttribute("id",usersEntity.getId());
        session.setAttribute("u_id",usersEntity.getuId());

        /*id = String.valueOf(usersEntity.getId());
        u_id = usersEntity.getuId();*/

    }

    public static void exit ()
    {
        /*id = null;
        u_id = null;*/
        session.removeAttribute("id");
        session.removeAttribute("u_id");
    }


    public static void restore ()
    {

    }

    public static boolean getStatus ()
    {
        boolean status = false;
        try {
            if ((session.getAttribute("id") != null) && (session.getAttribute("u_id") != null)) {
                status = true;
            }
        }catch (Exception er){
            status = false;
        }
        return status;
    }

    public static int getId () throws Exception {
        try {
            testAuntification();
            return Integer.valueOf(String.valueOf(session.getAttribute("id")));
        } catch (Exception er){
            throw new Exception(er.getMessage());
        }
    }

    public static String getUId () throws Exception {
        try {
            testAuntification();
            return String.valueOf(session.getAttribute("u_id"));
        } catch (Exception er){
            throw new Exception(er.getMessage());
        }
    }

    private static void testAuntification () throws Exception {
        try {
            if ((session.getAttribute("id") == null) || (session.getAttribute("u_id") == null))
            {
                throw new Exception("Ви не авторизовані!.");
            }
        } catch (Exception er){
            throw new Exception(er.getMessage());
        }
    }

    public static JsonObject Registar (String data){
        User user = new User("",data);

        //JsonParser jsonParser = new JsonParser();
        //JsonObject jsonObject = jsonParser.parse(data).getAsJsonObject();
        JsonObject jsonObject = new JsonObject();

        return user.addUser(jsonObject);
    }

}
