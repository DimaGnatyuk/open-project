package engine;

import com.google.gson.JsonObject;
import engine.apiV1_0b.EngineMenyV1_0b;
import servlet.EngineSession;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Admin on 21.02.2016.
 */
public class Engine {
    private String responce = "";
    public Engine (HttpServletRequest request, String verApi,String meny, String option, String action, String data){

        if (EngineMenyV1_0b.VERSSION_ENGINE.compareToIgnoreCase(verApi) == 0)
        {
            EngineMenyV1_0b Api1_0b = new EngineMenyV1_0b(meny, option, action, data);
            this.responce = Api1_0b.apply();
        }else if (EngineSession.MENY.compareToIgnoreCase(meny) == 0){
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("option",option);
            if ("Auntification".compareToIgnoreCase(option) == 0){
                EngineSession.auntification(request,data);
                jsonObject.addProperty("Status",String.valueOf(EngineSession.getStatus()));
            }else if ("Exit".compareToIgnoreCase(option) == 0){
                EngineSession.exit();
            }else if("Restore".compareToIgnoreCase(option) == 0){
                EngineSession.restore();
            }else if("Register".compareToIgnoreCase(option) == 0){
                EngineSession.Registar(data);
            }else if("GetMyUId".compareToIgnoreCase(option) == 0){
                try {
                    jsonObject.addProperty("MyId",String.valueOf(EngineSession.getUId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else if("GetStatus".compareToIgnoreCase(option) == 0){
                jsonObject.addProperty("Status",String.valueOf(EngineSession.getStatus()));
            }
            this.responce = jsonObject.toString();
        }else{
            this.responce+= "Error verApi";
        }
    }



    public String getResponce (){
        return this.responce;
    }


}
