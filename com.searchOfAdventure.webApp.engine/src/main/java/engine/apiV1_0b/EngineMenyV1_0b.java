package engine.apiV1_0b;


import engine.apiV1_0b.meny.*;

/**
 * Created by Admin on 21.02.2016.
 */
public class EngineMenyV1_0b {
    public static final String VERSSION_ENGINE = "1.0b";
    private String meny;
    private String option;
    private String action;
    private String data;

    public EngineMenyV1_0b(String meny, String option, String action, String data){
        this.meny = meny;
        this.option = option;
        this.action = action;
        this.data = data;
    }

    public String apply (){
        String resoure = "";
        if (EngineMenyNew.MENY.compareToIgnoreCase(this.meny) == 0)
        {
            EngineMenyNew engineMenyNew = new EngineMenyNew ( option,  action, data);
            resoure = engineMenyNew.apply();
        }else if (EngineMenyEdit.MENY.compareToIgnoreCase(this.meny) == 0)
        {
            EngineMenyEdit engineMenyEdit = new EngineMenyEdit ( option,  action, data);
            resoure = engineMenyEdit.apply();
        }else if (EngineMenyDel.MENY.compareToIgnoreCase(this.meny) == 0)
        {
            EngineMenyDel engineMenyDel = new EngineMenyDel ( option,  action, data);
            resoure = engineMenyDel.apply();
        }else if (EngineMenyView.MENY.compareToIgnoreCase(this.meny) == 0)
        {
            EngineMenyView engineMenyView = new EngineMenyView ( option,  action, data);
            resoure = engineMenyView.apply();
        }else if (EngineMenySystem.MENY.compareToIgnoreCase(this.meny) == 0)
        {
            EngineMenySystem engineMenySystem = new EngineMenySystem ( option,  action, data);
        }else{
            resoure = "Not selected menu...";
        }

        return resoure;
    }
}
