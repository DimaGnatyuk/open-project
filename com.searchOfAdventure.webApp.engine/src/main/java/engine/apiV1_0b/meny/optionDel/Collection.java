package engine.apiV1_0b.meny.optionDel;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import component.SystemComponent;
import dabaBase.Impl.DAO.Entity.CollectionEntity;
import dabaBase.Impl.DAO.Entity.ContestantsEntity;
import dabaBase.Impl.DAO.Entity.InterescollectionEntity;
import dabaBase.Impl.DAO.Entity.UsersEntity;
import dabaBase.Service.Factory;
import engine.apiV1_0b.meny.EngineMeny;
import servlet.EngineSession;

import java.sql.Date;

/**
 * Created by Admin on 15.03.2016.
 */
public class Collection  implements EngineMeny {
    public static final String OPTION = "Collection";

    private String action;
    private String data;

    public Collection(String action, String data) {
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        JsonObject jsonObject = new JsonObject();
        if ("User".compareToIgnoreCase(this.action) == 0)
        {
            jsonObject = optionUser(jsonObject);
        }else if ("Interes".compareToIgnoreCase(this.action) == 0) {
            jsonObject = optionInteres(jsonObject);
        }else if ("Collection".compareToIgnoreCase(this.action) == 0) {
            jsonObject = optionCollection(jsonObject);
        }
        return jsonObject.toString();
    }

    public JsonObject optionCollection(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Collection");
        data = "{"
                + "idCollection: \"1\""
                + "}";

        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            int idCollection = mainObject.get("idCollection").getAsInt();
            CollectionEntity collectionEntity = Factory.getInstance().getCollectionDAO().getCollectionById(idCollection);
            if (collectionEntity.getIdadmin() != EngineSession.getId())
            {
                new Exception("Error Collection...");
            }

            Factory.getInstance().getCollectionDAO().delCollection(collectionEntity);
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    public JsonObject optionInteres (JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Interes");
        data = "{"
                + "Interes: \"kljklj\","
                + "idCollection: \"1\""
                + "}";

        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            int idCollection = mainObject.get("idCollection").getAsInt();
            String interes = mainObject.get("Interes").getAsString();

            CollectionEntity collectionEntity = Factory.getInstance().getCollectionDAO().getCollectionByThisUser(idCollection);

            if (collectionEntity.getIdadmin() != EngineSession.getId()){
                new Exception("Error id Collection");
            }

            InterescollectionEntity interescollectionEntity = Factory.getInstance().getInteresCollectionDAO().getInteresByCollectionIdAndInteres(collectionEntity.getId(),interes);

            Factory.getInstance().getInteresCollectionDAO().delInteres(interescollectionEntity);
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    public JsonObject optionUser (JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Interes");
        data = "{"
                + "idUser: \"\","
                + "idCollection: \"1\""
                + "}";

        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            int idCollection = mainObject.get("idCollection").getAsInt();
            String idUser = mainObject.get("idUser").getAsString();//u_id

            CollectionEntity collectionEntity = Factory.getInstance().getCollectionDAO().getCollectionByThisUser(idCollection);
            UsersEntity usersEntity = Factory.getInstance().getUserDAO().getUserByU_id(idUser);

            if (collectionEntity.getIdadmin() != EngineSession.getId()){
                new Exception("Error id Collection");
            }
            ContestantsEntity contestantsEntity = Factory.getInstance().getContestantsDAO().getContestantsByIdCollectionAndIdUser(idCollection,usersEntity.getId());
            Factory.getInstance().getContestantsDAO().delContestants(contestantsEntity);
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }
}
