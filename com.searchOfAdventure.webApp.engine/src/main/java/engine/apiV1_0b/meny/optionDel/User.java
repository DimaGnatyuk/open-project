package engine.apiV1_0b.meny.optionDel;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dabaBase.Impl.DAO.Entity.*;
import dabaBase.Service.Factory;
import engine.apiV1_0b.meny.EngineMeny;
import servlet.EngineSession;

/**
 * Created by Admin on 15.03.2016.
 */
public class User  implements EngineMeny {
    public static final String OPTION = "User";
    private String action;
    private String data;

    public User(String action, String data) {
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        JsonObject jsonObject = new JsonObject();
        if ("User".compareToIgnoreCase(this.action) == 0)
        {
            jsonObject = optionUser(jsonObject);
        }else if ("Interes".compareToIgnoreCase(this.action) == 0) {
            jsonObject = optionInteres(jsonObject);
        }else if ("Collection".compareToIgnoreCase(this.action) == 0) {
            jsonObject = optionCollection(jsonObject);
        }else if ("Album".compareToIgnoreCase(this.action) == 0) {
            jsonObject = optionAlbum(jsonObject);
        }else if ("Photo".compareToIgnoreCase(this.action) == 0) {
            jsonObject = optionPhoto(jsonObject);
        }else if ("Friends".compareToIgnoreCase(this.action) == 0) {
            jsonObject = optionFriends(jsonObject);
        }else if ("Message".compareToIgnoreCase(this.action) == 0) {
            jsonObject = optionMessage(jsonObject);
        }
        return jsonObject.toString();
    }

    public JsonObject optionUser(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "User");

        try {
            JsonParser parser = new JsonParser();
            Factory.getInstance().getUserDAO().delUser(Factory.getInstance().getUserDAO().getUserById(EngineSession.getId()));
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    public JsonObject optionCollection(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Collection");
        data = "{"
                + "idCollection: \"1\""
                + "}";

        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            int idCollection = mainObject.get("idCollection").getAsInt();
            ContestantsEntity contestantsEntity = Factory.getInstance().getContestantsDAO().getContestantsByIdCollectionAndIdUser(idCollection,EngineSession.getId());
            Factory.getInstance().getContestantsDAO().delContestants(contestantsEntity);
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    public JsonObject optionAlbum(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Album");
        data = "{"
                + "idAlbum: \"1\""
                + "}";

        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            int idAlbum = mainObject.get("idAlbum").getAsInt();
            UseralbumEntity useralbumEntity = Factory.getInstance().getUserAlbumDAO().getAlbumById(idAlbum);
            if (useralbumEntity.getIduser() != EngineSession.getId())
            {
                new Exception("Error Collection...");
            }

            Factory.getInstance().getUserAlbumDAO().deleteAlbum(useralbumEntity);
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    public JsonObject optionPhoto(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Photo");
        data = "{"
                + "idPhoto: \"1\""
                + "}";

        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            int idCollection = mainObject.get("idCollection").getAsInt();
            CollectionEntity collectionEntity = Factory.getInstance().getCollectionDAO().getCollectionById(idCollection);
            if (collectionEntity.getIdadmin() != EngineSession.getId())
            {
                new Exception("Error Collection...");
            }

            Factory.getInstance().getCollectionDAO().delCollection(collectionEntity);
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    public JsonObject optionFriends(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Friends");
        data = "{"
                + "idFriends: \"1\""
                + "}";

        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            String Friends = mainObject.get("idFriends").getAsString();
            int idFriends = Factory.getInstance().getUserDAO().getUserByU_id(Friends).getId();
            FriendsEntity friendsEntity = Factory.getInstance().getUserFriendsDAO().getFriendsByIdUser(idFriends);
            Factory.getInstance().getUserFriendsDAO().delFriends(friendsEntity);
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    public JsonObject optionMessage(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Message");
        data = "{"
                + "idMessage: \"1\""
                + "}";

        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            int idMessage = mainObject.get("idMessage").getAsInt();
            MessageFriendEntity messageFriendEntity = Factory.getInstance().getMessageFriendsDAO().getMessageById(idMessage);
            int myId = EngineSession.getId();
            if (!((messageFriendEntity.getSender()==myId)||(messageFriendEntity.getReceiver()==myId)))
            {
                new Exception("Error Collection...");
            }

            Factory.getInstance().getMessageFriendsDAO().delMessage(messageFriendEntity);
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    public JsonObject optionInteres(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Interes");
        data = "{"
                + "Interes: \"1\""
                + "}";

        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            String interes = mainObject.get("Interes").getAsString();
            InteresuserEntity interesuserEntity = Factory.getInstance().getInteresUserDAO().getMyInteresByIdInteres(interes);
            if (interesuserEntity.getIduser() != EngineSession.getId())
            {
                new Exception("Error Collection...");
            }

            Factory.getInstance().getInteresUserDAO().delInteres(interesuserEntity);
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }


}
