package engine.apiV1_0b.meny.optionNew;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.org.apache.xerces.internal.impl.Constants;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import dabaBase.Impl.DAO.Entity.*;
import engine.apiV1_0b.meny.EngineMeny;
import dabaBase.Service.Factory;
import org.apache.commons.codec.digest.Crypt;
import org.apache.commons.codec.digest.DigestUtils;
import servlet.EngineSession;
import systemFunction.SystemFunction;

import java.sql.Timestamp;


/**
 * Created by Admin on 21.02.2016.
 */
public class User implements EngineMeny {
    public static final String OPTION = "User";

    private String action;
    private String data;
    public User (String action, String data){
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        JsonObject jsonObject = new JsonObject();
        if ("User".compareToIgnoreCase(action) == 0) //Створення нового користувача
        {
            //user(jsonObject); deprecated
            jsonObject = addUser(jsonObject);
        }else if("Album".compareToIgnoreCase(action) == 0){
            jsonObject = addAlbum(jsonObject);

        }else if("Photo".compareToIgnoreCase(action) == 0){

        }else if("Friend".compareToIgnoreCase(action) == 0){
            jsonObject = addFriend (jsonObject);

        }else if("Message".compareToIgnoreCase(action) == 0){
            jsonObject = addMessage (jsonObject);

        }else if("Interes".compareToIgnoreCase(action) == 0){
            jsonObject = addInteres(jsonObject);

        }else if("Collection".compareToIgnoreCase(action) == 0){
            jsonObject = addCollection (jsonObject);
        }
        return jsonObject.toString();
    }

    public JsonObject addUser (JsonObject jsonObject){
        try {
            jsonObject.addProperty("Action", "User");//JSON - що буде повернуто відправнику

            data = "{Name: \"Dima Gnatyuk\","
                    + "Email: \"dim_mon@ukr.net\","
                    + "Password: \"199717\","
                    + "Status: \"some string\","
                    + "Country: \"1\","
                    + "Sity: \"1\","
                    + "LanguageUi: \"ua\"}";

            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            JsonElement nameEntity = mainObject.get("Name");
            JsonElement emailEntity = mainObject.get("Email");
            JsonElement passwordEntity = mainObject.get("Password");
            JsonElement statusEntity = mainObject.get("Status");
            JsonElement countryEntity = mainObject.get("Country");
            JsonElement sityEntity = mainObject.get("Sity");
            JsonElement languageUi = mainObject.get("LanguageUi");

            String u_id = DigestUtils.md5Hex(emailEntity.getAsString()+passwordEntity.getAsString());

            UsersEntity usersEntity = new UsersEntity();
            usersEntity.setName(nameEntity.getAsString());
            usersEntity.setEmail(emailEntity.getAsString());
            usersEntity.setStatusUser(statusEntity.getAsString());
            usersEntity.setuId(u_id);
            usersEntity.setLanguageUi(languageUi.getAsString());
            usersEntity.setCountry(countryEntity.getAsInt());
            usersEntity.setSity(sityEntity.getAsInt());

            long time = System.currentTimeMillis();
            usersEntity.setDataReg(new Timestamp(time));

            Factory.getInstance().getUserDAO().addUser(usersEntity);

            jsonObject.addProperty("Status", "0");
        }catch (Exception error)//обробка помилок
        {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            if (error.getMessage().toString().indexOf("u_id") != -1)
            {
                message="Такий користувач уже існує!.";
            }else if (error.getMessage().toString().indexOf("for key 'email'") != -1)
            {
                message="Такий Email уже існує!.";
            }

            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject addAlbum (JsonObject jsonObject){
        try {
            jsonObject.addProperty("Action", "Album");//JSON - що буде повернуто відправнику

            data = "{"
                    + "Title: \"dim_mon@ukr.net\","
                    + "Description: \"199717\","
                    + "Enabled: \"1\""
                    +"}";
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            JsonElement titleEntity = mainObject.get("Title");
            JsonElement descriptionEntity = mainObject.get("Description");
            JsonElement enableEntity = mainObject.get("Enabled");

            UseralbumEntity useralbumEntity = new UseralbumEntity();

            useralbumEntity.setName(titleEntity.getAsString());
            useralbumEntity.setTitle(descriptionEntity.getAsString());
            useralbumEntity.setEnabled(enableEntity.getAsInt());

            useralbumEntity.setIduser(EngineSession.getId());

            Factory.getInstance().getUserAlbumDAO().addAlbum(useralbumEntity);
            jsonObject.addProperty("Status", "0");
        }catch (Exception error)
        {
            jsonObject.addProperty("Status", "Error");
            String message=error.getMessage();

            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject addFriend (JsonObject jsonObject)
    {
        data = "{"
                + "IdPeople: \"dim_mon@ukr.net\""
                +"}";
        try {
            jsonObject.addProperty("Action", "Friend");//JSON - що буде повернуто відправнику

            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();
            JsonElement idPeoples = mainObject.get("IdPeople");

            int myId = Integer.valueOf(EngineSession.getId());
            int idPeople = Integer.valueOf(idPeoples.getAsString());

            FriendsEntity friendsEntity = new FriendsEntity();

            friendsEntity.setIdUserReqeust(myId);
            friendsEntity.setIdUserResponce(idPeople);
            friendsEntity.setMdsum(SystemFunction.MD5(Math.sin(myId)+Math.sin(idPeople)));

            Factory.getInstance().getUserFriendsDAO().addFriends(friendsEntity);
            jsonObject.addProperty("Status", "0");
        }catch (Exception error)
        {
            jsonObject.addProperty("Status", "Error");
            String message=error.getMessage();

            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject addMessage (JsonObject jsonObject)
    {
        data = "{"
                + "IdFriends: \"dim_mon@ukr.net\","
                + "Message: \"dim_mon@ukr.net\""
                +"}";
        try {
            jsonObject.addProperty("Action", "Message");//JSON - що буде повернуто відправнику

            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();
            JsonElement idFriends = mainObject.get("IdFriends");
            JsonElement message = mainObject.get("Message");

            int myId = Integer.valueOf(EngineSession.getId());
            int idFriend = Integer.valueOf(idFriends.getAsString());
            String text = message.getAsString();

            MessageFriendEntity messageFriendEntity = new MessageFriendEntity();
            messageFriendEntity.setMessage(text);

            messageFriendEntity.setSender(myId);
            messageFriendEntity.setReceiver(idFriend);
            messageFriendEntity.setSummmd(SystemFunction.MD5(Math.sin(myId)+Math.sin(idFriend)));

            Factory.getInstance().getMessageFriendsDAO().addMessage(messageFriendEntity);
            jsonObject.addProperty("Status", "0");
        }catch (Exception error)
        {
            jsonObject.addProperty("Status", "Error");
            String message=error.getMessage();

            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject addInteres (JsonObject jsonObject){
        data = "{"
                + "Interes: \"Покатушки\""
                +"}";
        try {
            jsonObject.addProperty("Action", "Interes");//JSON - що буде повернуто відправнику

            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();
            JsonElement interes = mainObject.get("Interes");

            IntereslangnameEntity intereslangnameEntity = Factory.getInstance().getInteresNameDAO().getInteresNameByName(interes.getAsString());
            int myId = Integer.valueOf(EngineSession.getId());

            InteresuserEntity interesuserEntity = new InteresuserEntity();
            interesuserEntity.setIduser(myId);
            interesuserEntity.setIdinteres(intereslangnameEntity.getIdintere());

            Factory.getInstance().getInteresUserDAO().addInteres(interesuserEntity);
            jsonObject.addProperty("Status", "0");
        }catch (Exception error)
        {
            jsonObject.addProperty("Status", "Error");
            String message=error.getMessage();

            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject addCollection (JsonObject jsonObject)
    {
        data = "{"
                + "IdCollection: \"id\""
                +"}";
        try {
            jsonObject.addProperty("Action", "Collection");//JSON - що буде повернуто відправнику

            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();
            JsonElement IdCollection = mainObject.get("IdCollection");

            int myId = Integer.valueOf(EngineSession.getId());

            ContestantsEntity contestantsEntity = new ContestantsEntity();
            contestantsEntity.setIduser(myId);
            contestantsEntity.setIdcollection(IdCollection.getAsInt());

            Factory.getInstance().getContestantsDAO().addContestants(contestantsEntity);
            jsonObject.addProperty("Status", "0");
        }catch (Exception error)
        {
            jsonObject.addProperty("Status", "Error");
            String message=error.getMessage();

            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

}
