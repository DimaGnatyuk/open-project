package engine.apiV1_0b.meny.optionNew;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import component.SystemComponent;
import dabaBase.Impl.DAO.Entity.CollectionEntity;
import dabaBase.Impl.DAO.Entity.InterescollectionEntity;
import dabaBase.Impl.DAO.Entity.IntereslangnameEntity;
import dabaBase.Impl.DAO.Entity.InteresuserEntity;
import dabaBase.Service.Factory;
import engine.apiV1_0b.meny.EngineMeny;
import servlet.EngineSession;

import java.sql.Date;

/**
 * Created by Admin on 21.02.2016.
 */
public class Collection implements EngineMeny {
    public static final String OPTION = "Collection";

    private String action;
    private String data;
    public Collection (String action, String data){
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        JsonObject jsonObject = new JsonObject();
        if ("Collection".compareToIgnoreCase(this.action) == 0)
        {
            jsonObject = actionCollection(jsonObject);
        }else if ("Interes".compareToIgnoreCase(this.action) == 0) {
            jsonObject = actionInteres(jsonObject);
        }
        return jsonObject.toString();
    }

    private JsonObject actionCollection (JsonObject jsonObject){
        jsonObject.addProperty("Action", "Collection");
        data = "{"
                + "Name: \"Покатушки\","
                + "Word: \"Покатушки\","
                + "Logo: \"Покатушки\","
                + "Country: \"Україні\","
                + "Sity: \"Черкаси\","
                + "Addres: \"Покатушки\","
                + "Data: \"2016-03-15\","
                + "Title: \"Покатушки\""
                +"}";

        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();
            /*JsonElement interes = mainObject.get("Collection");*/

            CollectionEntity collectionEntity = new CollectionEntity();
            String name = mainObject.get("Name").getAsString();
            String word = mainObject.get("Word").getAsString();
            String logo = mainObject.get("Logo").getAsString();
            String country_tmp = mainObject.get("Country").getAsString();
            String sity_tmp = mainObject.get("Sity").getAsString();
            String addres = mainObject.get("Addres").getAsString();
            String Title = mainObject.get("Title").getAsString();
            String dataOf_tmp = mainObject.get("Data").getAsString();
            int idAdmin = EngineSession.getId();
            int country = Factory.getInstance().getCountryNameDAO().getCountryNameByName(country_tmp).getIdcountry();
            int sity = Factory.getInstance().getSityNameDAO().getSityNameByName(country, sity_tmp).getIdsity();

            collectionEntity.setName(name);
            collectionEntity.setWord(word);
            collectionEntity.setLogo(logo);
            collectionEntity.setCountry(country);
            collectionEntity.setSity(sity);
            collectionEntity.setAdress(addres);
            collectionEntity.setTitle(Title);
            collectionEntity.setIdadmin(idAdmin);

            collectionEntity.setDateof(Date.valueOf(dataOf_tmp));

            collectionEntity.setData(SystemComponent.getTimestamp());

            Factory.getInstance().getCollectionDAO().addCollection(collectionEntity);
            jsonObject.addProperty("Status", "0");
        }catch (Exception error)
        {
            jsonObject.addProperty("Status", "Error");
            String message=error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject actionInteres (JsonObject jsonObject){
        data = "{"
                + "CollectionId: \"7\","
                + "Interes: \"Прогулянк\""
                + "}";
        try {
            jsonObject.addProperty("Action", "Interes");//JSON - що буде повернуто відправнику

            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();
            JsonElement interes = mainObject.get("Interes");
            JsonElement collection = mainObject.get("CollectionId");

            int interesId = Factory.getInstance().getInteresNameDAO().getInteresNameByName(interes.getAsString()).getIdintere();
            int collectionId = collection.getAsInt();

            CollectionEntity list = Factory.getInstance().getCollectionDAO().getCollectionByThisUser(collectionId);

            if (list != null){
                InterescollectionEntity interescollectionEntity = new InterescollectionEntity();

                interescollectionEntity.setIdinteres(interesId);
                interescollectionEntity.setIdcollection(list.getId());

                Factory.getInstance().getInteresCollectionDAO().addInteres(interescollectionEntity);
                jsonObject.addProperty("Status", "0");
            }else{
                new Exception("Not Found Collection..");
            }

        }catch (Exception error)
        {
            jsonObject.addProperty("Status", "Error");
            String message=error.getMessage();

            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }


}
