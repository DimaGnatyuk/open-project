package engine.apiV1_0b.meny;

import engine.apiV1_0b.meny.optionView.*;

/**
 * Created by Admin on 21.02.2016.
 */
public class EngineMenyView implements EngineMeny  {
    public static String MENY = "View";

    private String option;
    private String action;
    private String data;

    public EngineMenyView (String option, String action, String data)
    {
        this.option = option;
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        String responce = "2121";
        System.out.println("logs: (View log...");
        if (Collection.OPTION.compareToIgnoreCase(this.option) == 0)
        {
            Collection collection = new Collection (action, data);
            responce = collection.apply();
        }else if (User.OPTION.compareToIgnoreCase(this.option) == 0){
            User user = new User (action, data);
            responce = user.apply();
        }else if (Interes.OPTION.compareToIgnoreCase(this.option) == 0){
            Interes interes = new Interes (action, data);
            responce = interes.apply();
        }else if (Category.OPTION.compareToIgnoreCase(this.option) == 0){
            Category category = new Category (action, data);
            responce = category.apply();
        }else if (Country.OPTION.compareToIgnoreCase(this.option) == 0){
            Country country = new Country (action, data);
            responce = country.apply();
        }else if (Sity.OPTION.compareToIgnoreCase(this.option) == 0){
            Sity sity = new Sity (action, data);
            responce = sity.apply();
        }
        return responce;
    }
}
