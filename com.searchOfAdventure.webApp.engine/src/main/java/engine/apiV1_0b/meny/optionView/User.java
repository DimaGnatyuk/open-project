package engine.apiV1_0b.meny.optionView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dabaBase.Impl.DAO.Entity.*;
import dabaBase.JsonPacking;
import dabaBase.Service.Factory;
import engine.apiV1_0b.meny.EngineMeny;
import org.apache.commons.logging.Log;
import servlet.EngineSession;

import java.util.List;

/**
 * Created by Admin on 15.03.2016.
 */
public class User implements EngineMeny {
    public static final String OPTION = "User";

    private String action;
    private String data;

    public User(String action, String data) {
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        JsonObject jsonObject = new JsonObject();
        if ("Informs".compareToIgnoreCase(action) == 0) //Створення нового користувача
        {
            jsonObject = actionUserInform(jsonObject);
        }else if ("Interes".compareToIgnoreCase(action) == 0){
            jsonObject = actionUserInteres(jsonObject);
        }else if ("Collections".compareToIgnoreCase(action) == 0){
            jsonObject = actionUserCollection(jsonObject);
        }else if ("Friends".compareToIgnoreCase(action) == 0){
            jsonObject = actionUserFriends(jsonObject);
        }else if ("Albums".compareToIgnoreCase(action) == 0){
            jsonObject = actionUserAlbums(jsonObject);
        }else if ("Messages".compareToIgnoreCase(action) == 0){
            jsonObject = actionUserMessages (jsonObject);
        }else if("Peoples".compareToIgnoreCase(action) == 0){
            jsonObject = actionUserPeople(jsonObject);
        }
        return jsonObject.toString();
    }

    private JsonObject actionUserInform(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "UserInform");
        try {
            int mid = EngineSession.getId();
            UsersEntity usersEntity = Factory.getInstance().getUserDAO().getUserById(mid);
            System.out.println(JsonPacking.toJson(usersEntity).toString());
            jsonObject.addProperty("Content", JsonPacking.toJson(usersEntity).toString());
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject actionUserInteres(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "UserInteres");
        data = "{"
                + "idUser: \"dfa6e4e5f8f88dcf737bf66160ba67d4\""
                +"}";
        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject object = jsonParser.parse(data).getAsJsonObject();
            String idUser = object.get("idUser").getAsString();
            UsersEntity usersEntity = Factory.getInstance().getUserDAO().getUserByU_id(idUser);
            List<InteresuserEntity> interesEntityList = Factory.getInstance().getInteresUserDAO().getAllInteresByIdUser(usersEntity.getId());
            for (InteresuserEntity interesuserEntity :interesEntityList) {
                System.out.println(interesuserEntity.getIdinteres());
            }
            jsonObject.addProperty("Content", JsonPacking.toJsonListUserInteres(interesEntityList).toString());
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject actionUserCollection(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "UserCollection");
        data = "{"
                + "idUser: \"dfa6e4e5f8f88dcf737bf66160ba67d4\""
                +"}";
        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject object = jsonParser.parse(data).getAsJsonObject();
            String idUser = object.get("idUser").getAsString();
            UsersEntity usersEntity = Factory.getInstance().getUserDAO().getUserByU_id(idUser);
            List<ContestantsEntity> contestantsEntityList = Factory.getInstance().getContestantsDAO().getContestantsByIdUser(usersEntity.getId());
            for (ContestantsEntity contestantsEntity :contestantsEntityList) {
                System.out.println(contestantsEntity.getIdcollection());
            }
            jsonObject.addProperty("Content", JsonPacking.toJsonContestenys(contestantsEntityList).toString());
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject actionUserFriends(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "UserFriends");
        data = "{"
                + "idUser: \"dfa6e4e5f8f88dcf737bf66160ba67d4\""
                +"}";
        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject object = jsonParser.parse(data).getAsJsonObject();
            String uidUser = object.get("idUser").getAsString();

            List<FriendsEntity> friendsEntityList = Factory.getInstance().getUserFriendsDAO().getFriendsByIdPeople(uidUser);
            for (FriendsEntity friendsEntity:friendsEntityList) {
                System.out.println(friendsEntity.getId());
            }
            jsonObject.addProperty("Content", JsonPacking.toJsonListUserFriends(friendsEntityList).toString());
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject actionUserPeople(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "People");
        try {
            List<UsersEntity> usersEntityList = Factory.getInstance().getUserDAO().getAllUser();
            for (UsersEntity usersEntity:usersEntityList) {
                System.out.println(usersEntity.getId());
            }
            jsonObject.addProperty("Content", JsonPacking.toJsonListUser(usersEntityList).toString());
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject actionUserAlbums (JsonObject jsonObject) {
        jsonObject.addProperty("Action", "UserAlbum");
        data = "{"
                + "idUser: \"dfa6e4e5f8f88dcf737bf66160ba67d4\""
                +"}";
        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject object = jsonParser.parse(data).getAsJsonObject();
            String uidUser = object.get("idUser").getAsString();

            List<UseralbumEntity> useralbumEntities = Factory.getInstance().getUserAlbumDAO().getAllAlbum(uidUser);
            for (UseralbumEntity useralbumEntity:useralbumEntities) {
                System.out.println(useralbumEntity.getId());
            }
            jsonObject.addProperty("Content", JsonPacking.toJsonListAlbum(useralbumEntities).toString());
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    private JsonObject actionUserMessages (JsonObject jsonObject) {
        jsonObject.addProperty("Action", "UserFriends");
        data = "{"
                + "idUser: \"dfa6e4e5f8f88dcf737bf66160ba67d4\""
                +"}";
        try {

            List<MessageFriendEntity> messageFriendEntityList = Factory.getInstance().getMessageFriendsDAO().getAllMessage();
            for (MessageFriendEntity messageFriendEntity:messageFriendEntityList) {
                System.out.println(messageFriendEntity.getId());
            }
            jsonObject.addProperty("Content", JsonPacking.toJsonListUserMessages(messageFriendEntityList).toString());
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }


}
