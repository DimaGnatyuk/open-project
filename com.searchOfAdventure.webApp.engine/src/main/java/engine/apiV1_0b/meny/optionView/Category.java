package engine.apiV1_0b.meny.optionView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dabaBase.Impl.DAO.Entity.CollectionEntity;
import dabaBase.Impl.DAO.Entity.KategorinteresEntity;
import dabaBase.JsonPacking;
import dabaBase.Service.Factory;
import engine.apiV1_0b.meny.EngineMeny;

import java.util.List;

/**
 * Created by Admin on 03.04.2016.
 */
public class Category  implements EngineMeny {
    public static final String OPTION = "Category";

    private String action;
    private String data;

    public Category(String action, String data) {
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        JsonObject jsonObject = new JsonObject();
        if ("All".compareToIgnoreCase(action) == 0)
        {
            jsonObject = actionCategoryAll(jsonObject);
        }
        return jsonObject.toString();
    }

    private JsonObject actionCategoryAll (JsonObject jsonObject){
        jsonObject.addProperty("Action", "All");
        try {
            List<KategorinteresEntity> allKategoryInteres = Factory.getInstance().getKategoryInteresDAO().getAllKategoryInteres();
            jsonObject.add("Content", JsonPacking.toJsonListCategory(allKategoryInteres));
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }
}
