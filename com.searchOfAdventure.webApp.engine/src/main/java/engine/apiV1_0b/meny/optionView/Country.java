package engine.apiV1_0b.meny.optionView;

import com.google.gson.JsonObject;
import dabaBase.Impl.DAO.Entity.CountryEntity;
import dabaBase.Impl.DAO.Entity.KategorinteresEntity;
import dabaBase.JsonPacking;
import dabaBase.Service.Factory;
import engine.apiV1_0b.meny.EngineMeny;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Admin on 03.04.2016.
 */
public class Country  implements EngineMeny {
    public static final String OPTION = "Country";

    private String action;
    private String data;

    public Country(String action, String data) {
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        JsonObject jsonObject = new JsonObject();
        if ("All".compareToIgnoreCase(action) == 0)
        {
            jsonObject = actionCountryAll(jsonObject);
        }
        return jsonObject.toString();
    }

    private JsonObject actionCountryAll (JsonObject jsonObject){
        jsonObject.addProperty("Action", "All");
        try {
            System.out.println("----->");
            List<CountryEntity> countryEntityList = Factory.getInstance().getCountryDAO().getAllCountry();
            System.out.println(countryEntityList.toString()+"<-----");
            jsonObject.add("Content", JsonPacking.toJsonListCountry(countryEntityList));
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }
}
