package engine.apiV1_0b.meny.optionView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import component.SystemComponent;
import dabaBase.Impl.DAO.Entity.*;
import dabaBase.JsonPacking;
import dabaBase.Service.Factory;
import engine.apiV1_0b.meny.EngineMeny;
import servlet.EngineSession;

import java.sql.Date;
import java.util.List;

/**
 * Created by Admin on 15.03.2016.
 */
public class Collection implements EngineMeny {
    public static final String OPTION = "Collection";

    private String action;
    private String data;

    public Collection(String action, String data) {
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        JsonObject jsonObject = new JsonObject();
        if ("CollectionById".compareToIgnoreCase(action) == 0) //Створення нового користувача
        {
            jsonObject = actionCollectionById(jsonObject);
        } else if ("All".compareToIgnoreCase(action) == 0) {
            jsonObject = actionCollection(jsonObject);
        }
        return jsonObject.toString();
    }

    private JsonObject actionCollectionById(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Collection");
        data = "{"
                + "idCollection: \"Покатушки\""
                + "}";
        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();
            int idCollection = mainObject.get("idCollection").getAsInt();
            CollectionEntity collectionEntity = Factory.getInstance().getCollectionDAO().getCollectionById(idCollection);
            jsonObject.addProperty("Content", JsonPacking.toJson(collectionEntity).toString());
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }


    private JsonObject actionCollection(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Collection");
        try {
            System.out.println("Logs View->Collection...");
            List<CollectionEntity> collectionEntityList = Factory.getInstance().getCollectionDAO().getAllCollection();
            jsonObject.add("Content", JsonPacking.toJsonListCollection(collectionEntityList));
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }
}
