package engine.apiV1_0b.meny.optionView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dabaBase.Impl.DAO.Entity.CollectionEntity;
import dabaBase.Impl.DAO.Entity.InteresEntity;
import dabaBase.Impl.DAO.Entity.KategorlangnameEntity;
import dabaBase.JsonPacking;
import dabaBase.Service.Factory;
import engine.apiV1_0b.meny.EngineMeny;

import java.util.List;

/**
 * Created by Admin on 15.03.2016.
 */
public class Interes implements EngineMeny {
    public static final String OPTION = "Interes";

    private String action;
    private String data;

    public Interes(String action, String data) {
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        JsonObject jsonObject = new JsonObject();
        if ("All".compareToIgnoreCase(action) == 0)
        {
            jsonObject = actionInteres(jsonObject);
        }
        return jsonObject.toString();
    }

    private JsonObject actionInteres(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Interes");
        data = "{"
                + "Category: \"Tesn\""
                +"}";
        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject object = jsonParser.parse(data).getAsJsonObject();
            String nameCategory = object.get("Category").getAsString();
            KategorlangnameEntity kategorlangnameEntity = Factory.getInstance().getKategoryInteresDAO().getKategoryInteresByName(nameCategory);
            List<InteresEntity> interesEntityList = Factory.getInstance().getInteresDAO().getAllInteresEntityByKateguri(kategorlangnameEntity);
            jsonObject.addProperty("Content", JsonPacking.toJsonListInteres(interesEntityList).toString());
            jsonObject.addProperty("Status", "0");
        } catch (Exception error) {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }
}
