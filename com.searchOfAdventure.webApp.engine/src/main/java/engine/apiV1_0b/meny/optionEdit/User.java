package engine.apiV1_0b.meny.optionEdit;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dabaBase.Impl.DAO.Entity.UseralbumEntity;
import dabaBase.Impl.DAO.Entity.UsersEntity;
import dabaBase.Service.Factory;
import engine.Engine;
import engine.apiV1_0b.meny.EngineMeny;
import org.apache.commons.codec.digest.DigestUtils;
import servlet.EngineSession;

import java.sql.Timestamp;

/**
 * Created by Admin on 15.03.2016.
 */
public class User implements EngineMeny {
    public static final String OPTION = "User";
    private String action;
    private String data;

    public User(String action, String data) {
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        JsonObject jsonObject = new JsonObject();
        if ("User".compareToIgnoreCase(this.action) == 0)
        {
            jsonObject = optionUser(jsonObject);
        }else if ("Album".compareToIgnoreCase(this.action) == 0) {
            jsonObject = optionAlbum(jsonObject);
        }
        return jsonObject.toString();
    }

    public JsonObject optionUser(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "User");
        data = "{Name: \"Dima Gnatyuk\","
                + "Email: \"dim_mon@ukr.net\","
                + "Password: \"199717\","
                + "Status: \"some string\","
                + "Country: \"1\","
                + "Sity: \"1\","
                + "LanguageUi: \"ua\"}";
        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            JsonElement nameEntity = mainObject.get("Name");
            JsonElement emailEntity = mainObject.get("Email");
            JsonElement passwordEntity = mainObject.get("Password");
            JsonElement statusEntity = mainObject.get("Status");
            JsonElement countryEntity = mainObject.get("Country");
            JsonElement sityEntity = mainObject.get("Sity");
            JsonElement languageUi = mainObject.get("LanguageUi");

            String u_id = DigestUtils.md5Hex(emailEntity.getAsString() + passwordEntity.getAsString());
            UsersEntity usersEntity = Factory.getInstance().getUserDAO().getUserById(EngineSession.getId());
            if (usersEntity.getuId() != u_id)
            {
                new Exception("Error passwords...");
            }

            usersEntity.setName(nameEntity.getAsString());
            usersEntity.setStatusUser(statusEntity.getAsString());
            usersEntity.setLanguageUi(languageUi.getAsString());
            usersEntity.setCountry(countryEntity.getAsInt());
            usersEntity.setSity(sityEntity.getAsInt());

            Factory.getInstance().getUserDAO().updateUser(usersEntity);

            jsonObject.addProperty("Status", "0");
        } catch (Exception error)//обробка помилок
        {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }

    public JsonObject optionAlbum(JsonObject jsonObject) {
        jsonObject.addProperty("Action", "Album");
        data = "{"
                + "idAlbum: \"1\","
                + "Title: \"dim_mon@ukr.net\","
                + "Description: \"199717\","
                + "Enabled: \"1\""
                +"}";
        try {
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(data).getAsJsonObject();

            JsonElement idAlbumEntity = mainObject.get("idAlbum");
            JsonElement titleEntity = mainObject.get("Title");
            JsonElement descriptionEntity = mainObject.get("Description");
            JsonElement enableEntity = mainObject.get("Enabled");

            UseralbumEntity useralbumEntity = Factory.getInstance().getUserAlbumDAO().getAlbumById(EngineSession.getId());
            if (useralbumEntity.getIduser() != EngineSession.getId())
            {
                new Exception("Error album...");
            }
            useralbumEntity.setName(titleEntity.getAsString());
            useralbumEntity.setTitle(descriptionEntity.getAsString());
            useralbumEntity.setEnabled(enableEntity.getAsInt());


            Factory.getInstance().getUserAlbumDAO().updateAlbum(useralbumEntity);
            jsonObject.addProperty("Status", "0");
        } catch (Exception error)//обробка помилок
        {
            jsonObject.addProperty("Status", "Error");
            String message = error.getMessage();
            jsonObject.addProperty("Description", message);
        }
        return jsonObject;
    }
}
