package engine.apiV1_0b.meny;

import engine.apiV1_0b.meny.optionDel.Collection;
import engine.apiV1_0b.meny.optionDel.User;

/**
 * Created by Admin on 21.02.2016.
 */
public class EngineMenyDel implements EngineMeny {
    public static String MENY = "Del";

    private String option;
    private String action;
    private String data;

    public EngineMenyDel (String option, String action, String data)
    {
        this.option = option;
        this.action = action;
        this.data = data;
    }

    @Override
    public String apply() {
        String responce = "";
        if (Collection.OPTION.compareToIgnoreCase(this.option) == 0)
        {
            Collection collection = new Collection (action, data);
            responce = collection.apply();
        }else if (User.OPTION.compareToIgnoreCase(this.option) == 0){
            User user = new User (action, data);
            responce = user.apply();
        }
        return responce;
    }
}
