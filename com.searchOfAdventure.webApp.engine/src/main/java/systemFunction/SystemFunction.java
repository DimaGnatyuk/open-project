package systemFunction;

import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by Admin on 28.02.2016.
 */
public class SystemFunction {

    public static String MD5 (int value)
    {
        return MD5 (String.valueOf(value));
    }

    public static String MD5 (Double value)
    {
        return MD5 (String.valueOf(value));
    }

    public static String MD5 (Float value)
    {
        return MD5 (String.valueOf(value));
    }

    public static String MD5 (Charsets value)
    {
        return MD5 (String.valueOf(value));
    }

    public static String MD5 (String string)
    {
        return DigestUtils.md5Hex(string);
    }
}
