package dabaBase.Service;

import dabaBase.Impl.*;
import dabaBase.Impl.DAO.*;

/**
 * Created by Admin on 27.02.2016.
 */
public class Factory {
    private static Factory instance = null;

    private UserDAO userDAO = null;
    private CollectionDAO collectionDAO = null;
    private InteresDAO interesDAO = null;
    private InteresNameDAO interesNameDAO = null;
    private KategoryInteresDAO kategoryInteresDAO = null;
    private KategorNameDAO kategorNameDAO = null;
    private MessageFriendsDAO messageFriendsDAO = null;
    private InteresUserDAO interesUserDAO = null;
    private CollectionUserDAO collectionUserDAO = null;
    private UserFriendsDAO userFriendsDAO = null;
    private UserAlbumDAO userAlbumDAO = null;
    private UserPhotoDAO userPhotoDAO = null;
    private InteresCollectionDAO interesCollectionDAO = null;
    private CountryDAO countryDAO = null;
    private CountryNameDAO countryNameDAO = null;
    private SityDAO sityDAO = null;
    private SityNameDAO sityNameDAO = null;
    private LanguageDAO languageDAO = null;
    private ContestantsDAO contestantsDAO = null;



    public static synchronized Factory getInstance(){
        if (instance == null){
            instance = new Factory();
        }
        return instance;
    }

    public UserDAO getUserDAO(){
        if (userDAO == null){
            userDAO = new UserDAOImpl();
        }
        return userDAO;
    }

    public CollectionDAO getCollectionDAO(){
        if (collectionDAO == null){
            collectionDAO = new CollectionDAOImpl();
        }
        return collectionDAO;
    }

    public InteresDAO getInteresDAO (){
        if (interesDAO == null)
        {
            interesDAO = new InteresDAOImpl();
        }
        return interesDAO;
    }

    public InteresNameDAO getInteresNameDAO(){
        if (interesNameDAO == null)
        {
            interesNameDAO = new InteresNameDAOImpl();
        }
        return interesNameDAO;
    }

    public KategoryInteresDAO getKategoryInteresDAO (){
        if (kategoryInteresDAO == null)
        {
            kategoryInteresDAO = new KategoryInteresDAOImpl();
        }
        return kategoryInteresDAO;
    }

    public KategorNameDAO getKategorNameDAO(){
        if (kategorNameDAO == null)
        {
            kategorNameDAO = new KategorNameDAOImpl();
        }
        return kategorNameDAO;
    }

    public CountryDAO getCountryDAO (){
        if (countryDAO == null)
        {
            countryDAO = new CountryDAOImpl();
        }
        return countryDAO;
    }

    public CountryNameDAO getCountryNameDAO (){
        if (countryNameDAO == null)
        {
            countryNameDAO = new CountryNameDAOImpl();
        }
        return countryNameDAO;
    }

    public SityDAO getSityDAO (){
        if (sityDAO == null)
        {
            sityDAO = new SityDAOImpl();
        }
        return sityDAO;
    }

    public SityNameDAO getSityNameDAO (){
        if (sityNameDAO == null)
        {
            sityNameDAO = new SityNameDAOImpl();
        }
        return sityNameDAO;
    }

    public MessageFriendsDAO getMessageFriendsDAO (){
        if (messageFriendsDAO == null)
        {
            messageFriendsDAO = new MessageFriendsDAOImpl();
        }
        return messageFriendsDAO;
    }

    public InteresUserDAO getInteresUserDAO(){
        if (interesUserDAO == null)
        {
            interesUserDAO = new InteresUserDAOImpl();
        }
        return interesUserDAO;
    }

    public CollectionUserDAO getCollectionUserDAO () {
        if (collectionUserDAO == null)
        {
            collectionUserDAO = new CollectionUserDAOImpl();
        }
        return collectionUserDAO;
    }

    public UserFriendsDAO getUserFriendsDAO (){
        if (userFriendsDAO == null)
        {
            userFriendsDAO = new UserFriendsDAOImpl();
        }
        return userFriendsDAO;
    }

    public UserAlbumDAO getUserAlbumDAO (){
        if (userAlbumDAO == null)
        {
            userAlbumDAO = new UserAlbumDAOImpl();
        }
            return userAlbumDAO;
    }

    public UserPhotoDAO getUserPhotoDAO (){
        if (userPhotoDAO == null){
            userPhotoDAO = new UserPhotoDAOImpl();
        }
        return userPhotoDAO;
    }

    public InteresCollectionDAO getInteresCollectionDAO (){
        if (interesCollectionDAO == null)
        {
            interesCollectionDAO = new InteresCollectionDAOImpl();
        }
        return interesCollectionDAO;
    }

    public LanguageDAO getLanguageDAO(){
        if (languageDAO == null)
        {
            languageDAO = new LanguageDAOImpl();
        }
        return languageDAO;
    }

    public ContestantsDAO getContestantsDAO (){
        if (contestantsDAO == null)
        {
            contestantsDAO = new ContestantsDAOImpl();
        }
        return contestantsDAO;
    }

}
