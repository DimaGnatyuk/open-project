package dabaBase;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dabaBase.Impl.DAO.Entity.*;
import dabaBase.Impl.KategoryInteresDAOImpl;
import dabaBase.Service.Factory;
import servlet.EngineSession;
import systemFunction.SystemFunction;

import java.util.List;

/**
 * Created by Admin on 16.03.2016.
 */
public class JsonPacking {

    public static JsonObject toJsonListCollection (List<CollectionEntity> collectionEntityList)
    {
        JsonObject jsonObject = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        for (CollectionEntity collectionEntity:collectionEntityList) {
            jsonArray.add(toJson(collectionEntity));
        }
        jsonObject.add("Collection",jsonArray);
        return jsonObject;
    }

    public static JsonObject toJsonListCountry (List<CountryEntity> countryEntityList)
    {
        JsonObject jsonObject = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        for (CountryEntity countryEntity:countryEntityList) {
            jsonArray.add(toJson(countryEntity));
        }
        jsonObject.add("Collection",jsonArray);
        return jsonObject;
    }

    public static JsonObject toJson (CollectionEntity collectionEntity) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", collectionEntity.getId());
        jsonObject.addProperty("Name", collectionEntity.getName());
        jsonObject.addProperty("Logo", collectionEntity.getLogo());

        /*CountrylangnameEntity countryEntity = Factory.getInstance().getCountryNameDAO().getCountryNameById(collectionEntity.getCountry());
        SitylangnameEntity sitylangnameEntity = Factory.getInstance().getSityNameDAO().getSityNameById(collectionEntity.getSity());
        jsonObject.addProperty("Country", countryEntity.getName());
        jsonObject.addProperty("Sity", sitylangnameEntity.getName());*/

        jsonObject.addProperty("Addres", collectionEntity.getAdress());

        /*UsersEntity admin = Factory.getInstance().getUserDAO().getUserById(collectionEntity.getIdadmin());

        jsonObject.addProperty("Admin", admin.getId());*/
        jsonObject.addProperty("Data", collectionEntity.getDateof().toString());
        jsonObject.addProperty("DataAdd", collectionEntity.getDateof().toString());
        return jsonObject;
    }

    public static JsonObject toJson (UsersEntity usersEntity){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", usersEntity.getuId());
        jsonObject.addProperty("Name", usersEntity.getName());
        jsonObject.addProperty("Email", usersEntity.getEmail());
        jsonObject.addProperty("Status", usersEntity.getStatusUser());

        jsonObject.addProperty("Language", usersEntity.getLanguageUi());
        jsonObject.addProperty("Data", usersEntity.getDataActiv().toString());
        jsonObject.addProperty("DataAdd", usersEntity.getDataReg().toString());

        return jsonObject;
    }

    public static JsonArray toJsonListInteres (List<InteresEntity> interesEntity){
        JsonArray jsonArray = new JsonArray();
        for (InteresEntity interesEntity1:interesEntity) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("",toJson(interesEntity1));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public static JsonArray toJsonListUserInteres (List<InteresuserEntity> interesuserEntities){

        JsonArray jsonArray = new JsonArray();
        for (InteresuserEntity interesuserEntity:interesuserEntities) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("Interes_id",toJsonInteresById(interesuserEntity.getIdinteres()));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public static JsonArray toJsonListCategory (List<KategorinteresEntity> kategorinteresEntities){

        JsonArray jsonArray = new JsonArray();
        for (KategorinteresEntity kategorinteresEntity:kategorinteresEntities) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("Kategory",toJsonCategoryById(kategorinteresEntity));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public static JsonArray toJsonListUserFriends (List<FriendsEntity> friendsEntityList) throws Exception {

        JsonArray jsonArray = new JsonArray();
        for (FriendsEntity friendsEntity:friendsEntityList) {
            System.out.println(friendsEntity.getMdsum());
            JsonObject jsonObject = new JsonObject();
            if (friendsEntity.getIdUserReqeust() != EngineSession.getId()) {
                jsonObject.add("Friends", toJsonFriendsById(friendsEntity.getIdUserReqeust()));
            }else{
                jsonObject.add("Friends", toJsonFriendsById(friendsEntity.getIdUserResponce()));
            }
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public static JsonArray toJsonListUserMessages (List<MessageFriendEntity> messageFriendEntityList) throws Exception {

        JsonArray jsonArray = new JsonArray();
        for (MessageFriendEntity messageFriendEntity:messageFriendEntityList) {
            System.out.println(messageFriendEntity.getMessage());
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("Message", toJsonMessage(messageFriendEntity));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public static JsonArray toJsonListUser (List<UsersEntity> usersEntityList){
        JsonArray jsonArray = new JsonArray();
        for (UsersEntity usersEntity : usersEntityList) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("",toJson(usersEntity));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public static JsonArray toJsonListAlbum (List<UseralbumEntity> useralbumEntitiesList){
        JsonArray jsonArray = new JsonArray();
        for (UseralbumEntity useralbumEntity : useralbumEntitiesList) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("",toJson(useralbumEntity));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public static JsonObject toJsonInteresById (int interesId){
        IntereslangnameEntity intereslangnameEntity = Factory.getInstance().getInteresNameDAO().getInteresNameById(interesId);
        if (intereslangnameEntity != null) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", interesId);
            jsonObject.addProperty("Name", intereslangnameEntity.getName());
            return jsonObject;
        }
        return null;
    }

    public static JsonObject toJsonCategoryById (KategorinteresEntity kategorinteresEntity){
        KategorlangnameEntity kategorlangnameEntity = Factory.getInstance().getKategorNameDAO().getKategorNameById(kategorinteresEntity.getId());
        if (kategorlangnameEntity != null) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", kategorlangnameEntity.getIdkategor());
            jsonObject.addProperty("Name", kategorlangnameEntity.getName());
            return jsonObject;
        }
        return null;
    }

    public static JsonObject toJsonFriendsById (int friendsId){
        UsersEntity usersEntity = Factory.getInstance().getUserDAO().getUserById(friendsId);
        if (usersEntity != null) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", usersEntity.getuId());
            jsonObject.addProperty("Name", usersEntity.getName());
            return jsonObject;
        }
        return null;
    }

    public static JsonObject toJsonMessage (MessageFriendEntity messageFriendEntity){
        if (messageFriendEntity != null) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", messageFriendEntity.getId());
            jsonObject.addProperty("Receiver", messageFriendEntity.getReceiver());
            jsonObject.addProperty("Sender", messageFriendEntity.getSender());
            jsonObject.addProperty("Message", messageFriendEntity.getMessage());
            jsonObject.addProperty("Data", messageFriendEntity.getData().toString());
            return jsonObject;
        }
        return null;
    }

    public static JsonObject toJson (InteresEntity interesEntity){
        IntereslangnameEntity intereslangnameEntity = Factory.getInstance().getInteresNameDAO().getInteresNameById(interesEntity.getId());
        if (intereslangnameEntity != null) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", interesEntity.getId());
            jsonObject.addProperty("Name", intereslangnameEntity.getName());
            return jsonObject;
        }
        return null;
    }

    public static JsonObject toJson (LanguagesEntity languagesEntity){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id",languagesEntity.getId());
        jsonObject.addProperty("Name",languagesEntity.getName());
        jsonObject.addProperty("Teg",languagesEntity.getTeg());
        return jsonObject;
    }

    public static JsonObject toJson (CountryEntity countryEntity){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id",countryEntity.getId());
        CountrylangnameEntity countrylangnameEntity = Factory.getInstance().getCountryNameDAO().getCountryNameById(countryEntity.getId());
        jsonObject.addProperty("Name",countrylangnameEntity.getName());
        return jsonObject;
    }

    public static JsonObject toJson (SityEntity sityEntity){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id",sityEntity.getId());
        SitylangnameEntity sitylangnameEntity = Factory.getInstance().getSityNameDAO().getSityNameById(sityEntity.getId());
        jsonObject.addProperty("Name",sitylangnameEntity.getName());
        return jsonObject;
    }

    public static JsonObject toJson (MessageFriendEntity messageFriendEntity){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id",messageFriendEntity.getSummmd());
        jsonObject.addProperty("Message",messageFriendEntity.getMessage());
        jsonObject.addProperty("Sender",messageFriendEntity.getSender());
        jsonObject.addProperty("Receiver",messageFriendEntity.getReceiver());
        jsonObject.addProperty("Data",messageFriendEntity.getData().toString());
        return jsonObject;
    }

    public static JsonObject toJson (FriendsEntity friendsEntity) throws Exception {
        JsonObject jsonObject = new JsonObject();
        int idFriend = friendsEntity.getIdUserReqeust();
        if (idFriend == EngineSession.getId())
        {
            idFriend = friendsEntity.getIdUserResponce();
        }
        jsonObject.addProperty("id",friendsEntity.getId());
        UsersEntity friends = Factory.getInstance().getUserDAO().getUserById(idFriend);
        jsonObject.addProperty("Name",friends.getName());
        return jsonObject;
    }

    public static JsonObject toJson (UseralbumEntity useralbumEntity){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("Album_id", useralbumEntity.getId());
        jsonObject.addProperty("User_id",useralbumEntity.getIduser());
        jsonObject.addProperty("Name",useralbumEntity.getName());
        jsonObject.addProperty("Title",useralbumEntity.getTitle());
        return jsonObject;
    }

    public static JsonArray toJsonContestenys (List<ContestantsEntity> contestantsEntityList){
        JsonArray jsonArray = new JsonArray();
        for (ContestantsEntity contestantsEntity:contestantsEntityList) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("Contestants", toJson(contestantsEntity));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    public static JsonObject toJson (ContestantsEntity contestantsEntity){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id",contestantsEntity.getId());
        jsonObject.addProperty("User_id",contestantsEntity.getIduser());
        UsersEntity usersEntity = Factory.getInstance().getUserDAO().getUserById(contestantsEntity.getIduser());
        jsonObject.addProperty("Name",usersEntity.getName());
        jsonObject.addProperty("Collection",contestantsEntity.getIdcollection());
        return jsonObject;
    }

    public static JsonObject toJson (KategorinteresEntity kategorinteresEntity){
        JsonObject jsonObject = new JsonObject();
        KategorlangnameEntity kategorlangnameEntity = Factory.getInstance().getKategoryInteresDAO().getKategoryInteresById(kategorinteresEntity.getId());
        jsonObject.addProperty("id",kategorlangnameEntity.getIdkategor());
        jsonObject.addProperty("Name",kategorlangnameEntity.getIdkategor());
        return jsonObject;
    }
}
