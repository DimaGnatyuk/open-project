package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.UserphotoEntity;
import dabaBase.Impl.DAO.UserPhotoDAO;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Admin on 12.03.2016.
 */
public class UserPhotoDAOImpl implements UserPhotoDAO {
    @Override
    public void addPhoto(UserphotoEntity userphotoEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(userphotoEntity);
        session.getTransaction().commit();
    }

    @Override
    public void delPhoto(UserphotoEntity userphotoEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(userphotoEntity);
        session.getTransaction().commit();
    }

    @Override
    public UserphotoEntity getPhotoById(int idPhoto) {
        UserphotoEntity userphotoEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        userphotoEntity = (UserphotoEntity) session.load(UserphotoEntity.class,idPhoto);
        session.getTransaction().commit();
        return userphotoEntity;
    }

    @Override
    public List<UserphotoEntity> getPhotoByUser(int idAlbum) {
        List<UserphotoEntity> userphotoEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        userphotoEntityList = session.createCriteria(UserphotoEntity.class).add(Restrictions.eq("idalbum",idAlbum)).list();
        session.getTransaction().commit();
        return userphotoEntityList;
    }
}
