package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.LanguagesEntity;
import dabaBase.Impl.DAO.LanguageDAO;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Admin on 13.03.2016.
 */
public class LanguageDAOImpl implements LanguageDAO {
    @Override
    public void addLanguage(LanguagesEntity languagesEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(languagesEntity);
        session.getTransaction().commit();
    }

    @Override
    public void updateLanguage(LanguagesEntity languagesEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(languagesEntity);
        session.getTransaction().commit();

    }

    @Override
    public void delLanguage(LanguagesEntity languagesEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(languagesEntity);
        session.getTransaction().commit();
    }

    @Override
    public LanguagesEntity getLanguageById(int id) {
        LanguagesEntity languagesEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        languagesEntity = (LanguagesEntity) session.load(LanguagesEntity.class,id);
        session.getTransaction().commit();
        return languagesEntity;
    }

    @Override
    public LanguagesEntity getLanguageByTeg(String teg) {
        LanguagesEntity languagesEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        languagesEntity = (LanguagesEntity) session.createCriteria(LanguagesEntity.class).add(Restrictions.like("teg",teg)).uniqueResult();
        session.getTransaction().commit();
        return languagesEntity;
    }

    @Override
    public LanguagesEntity getLanguageByName(String name) {
        LanguagesEntity languagesEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        languagesEntity = (LanguagesEntity) session.createCriteria(LanguagesEntity.class).add(Restrictions.like("name",name)).uniqueResult();
        session.getTransaction().commit();
        return languagesEntity;
    }

    @Override
    public List<LanguagesEntity> getAllLanguage() {
        List<LanguagesEntity> languagesEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        languagesEntity = session.createCriteria(LanguagesEntity.class).add(Restrictions.eq("enabled",1)).list();
        session.getTransaction().commit();
        return languagesEntity;
    }

    @Override
    public List<LanguagesEntity> getFullAllLanguage() {
        List<LanguagesEntity> languagesEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        languagesEntity = session.createCriteria(LanguagesEntity.class).list();
        session.getTransaction().commit();
        return languagesEntity;
    }
}
