package dabaBase.Impl;

import dabaBase.Impl.DAO.ContestantsDAO;
import dabaBase.Impl.DAO.Entity.ContestantsEntity;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Admin on 13.03.2016.
 */
public class ContestantsDAOImpl implements ContestantsDAO {
    @Override
    public void addContestants(ContestantsEntity contestantsEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(contestantsEntity);
        session.getTransaction().commit();
    }

    @Override
    public void delContestants(ContestantsEntity contestantsEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(contestantsEntity);
        session.getTransaction().commit();
    }

    @Override
    public List<ContestantsEntity> getContestantsByIdUser(int idUser) {
        List<ContestantsEntity> contestantsEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        contestantsEntityList = session.createCriteria(ContestantsEntity.class).add(Restrictions.eq("iduser",idUser)).list();
        session.getTransaction().commit();
        return contestantsEntityList;
    }

    @Override
    public List<ContestantsEntity> getContestantsByIdCollection(int idCollection) {
        List<ContestantsEntity> contestantsEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        contestantsEntityList = session.createCriteria(ContestantsEntity.class).add(Restrictions.eq("idcollection",idCollection)).list();
        session.getTransaction().commit();
        return contestantsEntityList;
    }

    @Override
    public ContestantsEntity getContestantsByIdCollectionAndIdUser(int idCollection, int idUser) {
        ContestantsEntity contestantsEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        contestantsEntity = (ContestantsEntity) session.createCriteria(ContestantsEntity.class).add(Restrictions.eq("iduser",idUser)).add(Restrictions.eq("idcollection",idCollection)).uniqueResult();
        session.getTransaction().commit();
        return contestantsEntity;
    }
}
