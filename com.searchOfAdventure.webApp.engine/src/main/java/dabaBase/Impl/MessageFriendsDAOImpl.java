package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.MessageFriendEntity;
import dabaBase.Impl.DAO.MessageFriendsDAO;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public class MessageFriendsDAOImpl implements MessageFriendsDAO {
    @Override
    public void addMessage(MessageFriendEntity messageFriendEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(messageFriendEntity);
        session.getTransaction().commit();
    }

    @Override
    public void delMessage(MessageFriendEntity messageFriendEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(messageFriendEntity);
        session.getTransaction().commit();
    }

    @Override
    public List<MessageFriendEntity> getAllMessage() {
        List<MessageFriendEntity> messageFriendEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        messageFriendEntityList = session.createCriteria(MessageFriendEntity.class).list();
        session.getTransaction().commit();
        return messageFriendEntityList;
    }

    @Override
    public MessageFriendEntity getMessageById(int idMessageFriendEntity) {
        MessageFriendEntity messageFriendEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        messageFriendEntity = (MessageFriendEntity)session.load(MessageFriendEntity.class,idMessageFriendEntity);
        session.getTransaction().commit();
        return messageFriendEntity;
    }
}
