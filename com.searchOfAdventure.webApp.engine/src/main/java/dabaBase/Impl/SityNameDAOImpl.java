package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.SitylangnameEntity;
import dabaBase.Impl.DAO.SityNameDAO;
import dabaBase.Service.Factory;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created by Admin on 13.03.2016.
 */
public class SityNameDAOImpl implements SityNameDAO {
    @Override
    public void addSityName(SitylangnameEntity sitylangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(sitylangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public void updateSityName(SitylangnameEntity sitylangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(sitylangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public void dellSityName(SitylangnameEntity sitylangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(sitylangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public SitylangnameEntity getSityNameById(int idSity) {
        SitylangnameEntity sitylangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        sitylangnameEntity = (SitylangnameEntity) session.load(SitylangnameEntity.class,idSity);
        session.getTransaction().commit();
        return sitylangnameEntity;
    }

    @Override
    public SitylangnameEntity getSityNameByName(int idCountry, String nameSity) {
        SitylangnameEntity sitylangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        sitylangnameEntity = (SitylangnameEntity) session.createCriteria(SitylangnameEntity.class).add(Restrictions.like("name",nameSity)).uniqueResult();
        session.getTransaction().commit();
        if (idCountry == Factory.getInstance().getSityDAO().getSityById(sitylangnameEntity.getIdsity()).getIdcountry()){

        }else{
            new Exception("Invalid Sity...");
        }
        return sitylangnameEntity;
    }
}
