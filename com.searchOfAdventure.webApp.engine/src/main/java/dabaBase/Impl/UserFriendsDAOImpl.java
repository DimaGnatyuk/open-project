package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.FriendsEntity;
import dabaBase.Impl.DAO.Entity.UsersEntity;
import dabaBase.Impl.DAO.UserFriendsDAO;
import dabaBase.Service.Factory;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import servlet.EngineSession;
import systemFunction.SystemFunction;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public class UserFriendsDAOImpl implements UserFriendsDAO {
    @Override
    public void addFriends(FriendsEntity friendsEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(friendsEntity);
        session.getTransaction().commit();
    }

    @Override
    public void delFriends(FriendsEntity friendsEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(friendsEntity);
        session.getTransaction().commit();
    }

    @Override
    public void checkingFriends(FriendsEntity friendsEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(friendsEntity);
        session.getTransaction().commit();
    }

    @Override
    public List<FriendsEntity> getAllFriends() {
        List<FriendsEntity> friendsEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        friendsEntityList = session.createCriteria(FriendsEntity.class).add(Restrictions.eq("validation",1)).list();
        session.getTransaction().commit();
        return friendsEntityList;
    }

    @Override
    public List<FriendsEntity> getAllRequisition() {
        List<FriendsEntity> friendsEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        friendsEntityList = session.createCriteria(FriendsEntity.class).add(Restrictions.eq("validation",0)).list();
        session.getTransaction().commit();
        return friendsEntityList;
    }

    @Override
    public FriendsEntity getRequisitionById(int idRequisitionFriends) {
        FriendsEntity friendsEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        friendsEntity = (FriendsEntity)session.load(FriendsEntity.class,idRequisitionFriends);
        session.getTransaction().commit();
        return friendsEntity;
    }

    @Override
    public FriendsEntity getFriendsByIdUser(String idFriends) throws Exception {

        UsersEntity friendsUser = Factory.getInstance().getUserDAO().getUserByU_id(idFriends);
        int myId = EngineSession.getId();
        String md5 = SystemFunction.MD5(Math.sin(myId)+Math.sin(friendsUser.getId()));

        FriendsEntity friendsEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        friendsEntity = (FriendsEntity)session.createCriteria(FriendsEntity.class).add(Restrictions.eq("mdsum",md5)).uniqueResult();
        session.getTransaction().commit();
        return friendsEntity;
    }

    @Override
    public FriendsEntity getFriendsByIdUser(int idFriends) throws Exception {
        UsersEntity friendsUser = Factory.getInstance().getUserDAO().getUserById(idFriends);
        int myId = EngineSession.getId();
        String md5 = SystemFunction.MD5(Math.sin(myId)+Math.sin(friendsUser.getId()));

        FriendsEntity friendsEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        friendsEntity = (FriendsEntity)session.createCriteria(FriendsEntity.class).add(Restrictions.eq("mdsum",md5)).uniqueResult();
        session.getTransaction().commit();
        return friendsEntity;
    }

    @Override
    public List<FriendsEntity> getFriendsByIdPeople(String idPeople) {
        UsersEntity friendsUser = Factory.getInstance().getUserDAO().getUserByU_id(idPeople);

        List<FriendsEntity> friendsEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        friendsEntity = session.createCriteria(FriendsEntity.class).add(Restrictions.or(Restrictions.eq("idUserReqeust",friendsUser.getId()),Restrictions.eq("idUserResponce",friendsUser.getId()))).list();

        session.getTransaction().commit();
        return friendsEntity;
    }

}
