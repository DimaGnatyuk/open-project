package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.CountryEntity;
import dabaBase.Impl.DAO.Entity.CountrylangnameEntity;
import dabaBase.Impl.DAO.Entity.SitylangnameEntity;

/**
 * Created by Admin on 13.03.2016.
 */
public interface CountryNameDAO {
    void addCountryName (CountrylangnameEntity countryEntity);
    void updateCountryName (CountrylangnameEntity countryEntity);
    void dellCountryName (CountrylangnameEntity countryEntity);

    CountrylangnameEntity getCountryNameById (int idCountry);
    CountrylangnameEntity getCountryNameByName (String nameCountry);


}
