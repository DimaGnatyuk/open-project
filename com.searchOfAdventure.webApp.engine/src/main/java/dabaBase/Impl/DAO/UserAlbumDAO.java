package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.UseralbumEntity;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public interface UserAlbumDAO {
    void addAlbum (UseralbumEntity useralbumEntity);
    void updateAlbum (UseralbumEntity useralbumEntity);
    void deleteAlbum (UseralbumEntity useralbumEntity);

    UseralbumEntity getAlbumById (int idAlbum);
    List<UseralbumEntity> getAllAlbum (int idUser);

    List<UseralbumEntity> getAllAlbum(String idUser);
}
