package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.InteresEntity;
import dabaBase.Impl.DAO.Entity.IntereslangnameEntity;
import dabaBase.Impl.DAO.Entity.KategorlangnameEntity;
import engine.apiV1_0b.meny.optionNew.Collection;

import java.util.List;

/**
 * Created by Admin on 10.03.2016.
 */
public interface InteresDAO {
    void createInteres (InteresEntity interes);
    void destroyInteres (InteresEntity interes);

    void getAllIdInteres (int idKategori);
    void getAllIdInteres (KategorlangnameEntity Kategori);

    void addInteres (IntereslangnameEntity interes);
    void delInteres (IntereslangnameEntity interes);
    void updateInteres (IntereslangnameEntity interes);

    IntereslangnameEntity getInteresById (int id);
    IntereslangnameEntity getInteresByName (String name);

    List<InteresEntity> getAllInteresEntityByIdKateguri (int idKategori);
    List<InteresEntity> getAllInteresEntityByKateguri (KategorlangnameEntity Kategori);
}
