package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.InterescollectionEntity;

import java.util.List;

/**
 * Created by Admin on 12.03.2016.
 */
public interface InteresCollectionDAO {
    void addInteres(InterescollectionEntity interescollectionEntity);
    void delInteres(InterescollectionEntity interescollectionEntity);

    List<InterescollectionEntity> getInteresByCollectionId(int idCollection);
    InterescollectionEntity getInteresByCollectionIdAndInteres(int idCollection, String nameInteres);
}
