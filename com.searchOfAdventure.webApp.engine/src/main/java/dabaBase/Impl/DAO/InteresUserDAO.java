package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.InteresuserEntity;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public interface InteresUserDAO {
    void addInteres (InteresuserEntity interesuserEntity);
    void delInteres (InteresuserEntity interesuserEntity);
    List<InteresuserEntity> getAllInteresByIdUser (int idUser);
    InteresuserEntity getMyInteresByIdInteres (String Interes) throws Exception;
}
