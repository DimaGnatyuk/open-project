package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.SityEntity;

import java.util.List;

/**
 * Created by Admin on 13.03.2016.
 */
public interface SityDAO {
    void addSity (SityEntity sityEntity);
    void updateSity (SityEntity sityEntity);
    void dellSity (SityEntity sityEntity);

    SityEntity getSityById (int id);
    List<SityEntity> getAllSity (int idCountry);
}
