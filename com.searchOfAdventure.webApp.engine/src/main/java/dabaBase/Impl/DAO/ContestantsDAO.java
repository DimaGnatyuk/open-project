package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.ContestantsEntity;

import java.util.List;

/**
 * Created by Admin on 13.03.2016.
 */
public interface ContestantsDAO {
    void addContestants (ContestantsEntity contestantsEntity);
    void delContestants (ContestantsEntity contestantsEntity);

    List<ContestantsEntity> getContestantsByIdUser (int idUser);
    List<ContestantsEntity> getContestantsByIdCollection (int idCollection);
    ContestantsEntity getContestantsByIdCollectionAndIdUser (int idCollection, int idUser);
}
