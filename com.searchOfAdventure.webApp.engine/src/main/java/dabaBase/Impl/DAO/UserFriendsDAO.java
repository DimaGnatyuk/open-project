package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.FriendsEntity;
import dabaBase.Impl.DAO.Entity.UsersEntity;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public interface UserFriendsDAO {
    void addFriends (FriendsEntity friendsEntity);
    void delFriends (FriendsEntity friendsEntity);
    void checkingFriends (FriendsEntity friendsEntity);

    List<FriendsEntity> getAllFriends ();
    List<FriendsEntity>  getAllRequisition ();
    FriendsEntity getRequisitionById (int idRequisitionFriends);
    FriendsEntity getFriendsByIdUser (String idFriends) throws Exception;
    FriendsEntity getFriendsByIdUser (int idFriends) throws Exception;
    List<FriendsEntity> getFriendsByIdPeople(String idPeople);
}
