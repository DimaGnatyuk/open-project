package dabaBase.Impl.DAO.Entity;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Admin on 22.02.2016.
 */
public class CollectionEntity {
    private int id;
    private String name;
    private String word;
    private String logo;
    private int country;
    private int sity;
    private String adress;
    private Date dateof;
    private int idadmin;
    private String title;
    private Timestamp data;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public int getSity() {
        return sity;
    }

    public void setSity(int sity) {
        this.sity = sity;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Date getDateof() {
        return dateof;
    }

    public void setDateof(Date dateof) {
        this.dateof = dateof;
    }

    public int getIdadmin() {
        return idadmin;
    }

    public void setIdadmin(int idadmin) {
        this.idadmin = idadmin;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CollectionEntity that = (CollectionEntity) o;

        if (id != that.id) return false;
        if (country != that.country) return false;
        if (sity != that.sity) return false;
        if (idadmin != that.idadmin) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (word != null ? !word.equals(that.word) : that.word != null) return false;
        if (logo != null ? !logo.equals(that.logo) : that.logo != null) return false;
        if (adress != null ? !adress.equals(that.adress) : that.adress != null) return false;
        if (dateof != null ? !dateof.equals(that.dateof) : that.dateof != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (word != null ? word.hashCode() : 0);
        result = 31 * result + (logo != null ? logo.hashCode() : 0);
        result = 31 * result + country;
        result = 31 * result + sity;
        result = 31 * result + (adress != null ? adress.hashCode() : 0);
        result = 31 * result + (dateof != null ? dateof.hashCode() : 0);
        result = 31 * result + idadmin;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }
}
