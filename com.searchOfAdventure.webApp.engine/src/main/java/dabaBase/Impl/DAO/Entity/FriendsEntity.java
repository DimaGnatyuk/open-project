package dabaBase.Impl.DAO.Entity;
/**
 * Created by Admin on 22.02.2016.
 */
public class FriendsEntity {
    private int id;
    private String mdsum;
    private int idUserReqeust;
    private int idUserResponce;
    private Byte validation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMdsum() {
        return mdsum;
    }

    public void setMdsum(String mdsum) {
        this.mdsum = mdsum;
    }

    public int getIdUserReqeust() {
        return idUserReqeust;
    }

    public void setIdUserReqeust(int idUserReqeust) {
        this.idUserReqeust = idUserReqeust;
    }

    public int getIdUserResponce() {
        return idUserResponce;
    }

    public void setIdUserResponce(int idUserResponce) {
        this.idUserResponce = idUserResponce;
    }

    public Byte getValidation() {
        return validation;
    }

    public void setValidation(Byte validation) {
        this.validation = validation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FriendsEntity that = (FriendsEntity) o;

        if (id != that.id) return false;
        if (idUserReqeust != that.idUserReqeust) return false;
        if (idUserResponce != that.idUserResponce) return false;
        if (mdsum != null ? !mdsum.equals(that.mdsum) : that.mdsum != null) return false;
        if (validation != null ? !validation.equals(that.validation) : that.validation != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (mdsum != null ? mdsum.hashCode() : 0);
        result = 31 * result + idUserReqeust;
        result = 31 * result + idUserResponce;
        result = 31 * result + (validation != null ? validation.hashCode() : 0);
        return result;
    }
}
