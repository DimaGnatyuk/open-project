package dabaBase.Impl.DAO.Entity;

/**
 * Created by Admin on 22.02.2016.
 */
public class InteresEntity {
    private int id;
    private int idkategor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdkategor() {
        return idkategor;
    }

    public void setIdkategor(int idkategor) {
        this.idkategor = idkategor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InteresEntity that = (InteresEntity) o;

        if (id != that.id) return false;
        if (idkategor != that.idkategor) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idkategor;
        return result;
    }
}
