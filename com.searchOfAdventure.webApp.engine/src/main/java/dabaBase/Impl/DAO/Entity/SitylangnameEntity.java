package dabaBase.Impl.DAO.Entity;

/**
 * Created by Admin on 22.02.2016.
 */
public class SitylangnameEntity {
    private int id;
    private int idsity;
    private String name;
    private int lang;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdsity() {
        return idsity;
    }

    public void setIdsity(int idsity) {
        this.idsity = idsity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLang() {
        return lang;
    }

    public void setLang(int lang) {
        this.lang = lang;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SitylangnameEntity that = (SitylangnameEntity) o;

        if (id != that.id) return false;
        if (idsity != that.idsity) return false;
        if (lang != that.lang) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idsity;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + lang;
        return result;
    }
}
