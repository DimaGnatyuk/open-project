package dabaBase.Impl.DAO.Entity;

/**
 * Created by Admin on 22.02.2016.
 */
public class SityEntity {
    private int id;
    private int idcountry;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdcountry() {
        return idcountry;
    }

    public void setIdcountry(int idcountry) {
        this.idcountry = idcountry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SityEntity that = (SityEntity) o;

        if (id != that.id) return false;
        if (idcountry != that.idcountry) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idcountry;
        return result;
    }
}
