package dabaBase.Impl.DAO.Entity;

import java.sql.Timestamp;

/**
 * Created by Admin on 22.02.2016.
 */
public class MessageFriendEntity {
    private int id;
    private String summmd;
    private int sender;
    private int receiver;
    private String message;
    private Timestamp data;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSummmd() {
        return summmd;
    }

    public void setSummmd(String summmd) {
        this.summmd = summmd;
    }

    public int getSender() {
        return sender;
    }

    public void setSender(int sender) {
        this.sender = sender;
    }

    public int getReceiver() {
        return receiver;
    }

    public void setReceiver(int receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageFriendEntity that = (MessageFriendEntity) o;

        if (id != that.id) return false;
        if (sender != that.sender) return false;
        if (receiver != that.receiver) return false;
        if (summmd != null ? !summmd.equals(that.summmd) : that.summmd != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (summmd != null ? summmd.hashCode() : 0);
        result = 31 * result + sender;
        result = 31 * result + receiver;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }
}
