package dabaBase.Impl.DAO.Entity;

/**
 * Created by Admin on 22.02.2016.
 */
public class InterescollectionEntity {
    private int id;
    private int idcollection;
    private int idinteres;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdcollection() {
        return idcollection;
    }

    public void setIdcollection(int idcollection) {
        this.idcollection = idcollection;
    }

    public int getIdinteres() {
        return idinteres;
    }

    public void setIdinteres(int idinteres) {
        this.idinteres = idinteres;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InterescollectionEntity that = (InterescollectionEntity) o;

        if (id != that.id) return false;
        if (idcollection != that.idcollection) return false;
        if (idinteres != that.idinteres) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idcollection;
        result = 31 * result + idinteres;
        return result;
    }
}
