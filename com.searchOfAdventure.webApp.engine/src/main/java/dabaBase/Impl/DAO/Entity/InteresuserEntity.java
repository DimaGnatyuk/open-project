package dabaBase.Impl.DAO.Entity;
/**
 * Created by Admin on 22.02.2016.
 */
public class InteresuserEntity {
    private int id;
    private int iduser;
    private int idinteres;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public int getIdinteres() {
        return idinteres;
    }

    public void setIdinteres(int idinteres) {
        this.idinteres = idinteres;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InteresuserEntity that = (InteresuserEntity) o;

        if (id != that.id) return false;
        if (iduser != that.iduser) return false;
        if (idinteres != that.idinteres) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + iduser;
        result = 31 * result + idinteres;
        return result;
    }
}
