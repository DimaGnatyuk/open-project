package dabaBase.Impl.DAO.Entity;

import javax.persistence.Column;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Admin on 22.02.2016.
 */
public class UsersEntity {
    private int id;
    private String uId;
    private String name;
    private String statusUser;
    private String email;
    private Integer country;
    private Integer sity;
    private String languageUi;
    private Timestamp dataReg;
    private Date dataActiv;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatusUser() {
        return statusUser;
    }

    public void setStatusUser(String statusUser) {
        this.statusUser = statusUser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public Integer getSity() {
        return sity;
    }

    public void setSity(Integer sity) {
        this.sity = sity;
    }

    public String getLanguageUi() {
        return languageUi;
    }

    public void setLanguageUi(String languageUi) {
        this.languageUi = languageUi;
    }

    public Timestamp getDataReg() {
        return dataReg;
    }

    public void setDataReg(Timestamp dataReg) {
        this.dataReg = dataReg;
    }

    public Date getDataActiv() {
        return dataActiv;
    }

    public void setDataActiv(Date dataActiv) {
        this.dataActiv = dataActiv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersEntity that = (UsersEntity) o;

        if (id != that.id) return false;
        if (uId != null ? !uId.equals(that.uId) : that.uId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (statusUser != null ? !statusUser.equals(that.statusUser) : that.statusUser != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (sity != null ? !sity.equals(that.sity) : that.sity != null) return false;
        if (languageUi != null ? !languageUi.equals(that.languageUi) : that.languageUi != null) return false;
        if (dataReg != null ? !dataReg.equals(that.dataReg) : that.dataReg != null) return false;
        if (dataActiv != null ? !dataActiv.equals(that.dataActiv) : that.dataActiv != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (uId != null ? uId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (statusUser != null ? statusUser.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (sity != null ? sity.hashCode() : 0);
        result = 31 * result + (languageUi != null ? languageUi.hashCode() : 0);
        result = 31 * result + (dataReg != null ? dataReg.hashCode() : 0);
        result = 31 * result + (dataActiv != null ? dataActiv.hashCode() : 0);
        return result;
    }
}
