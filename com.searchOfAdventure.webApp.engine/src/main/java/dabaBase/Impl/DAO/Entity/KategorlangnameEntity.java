package dabaBase.Impl.DAO.Entity;

/**
 * Created by Admin on 22.02.2016.
 */
public class KategorlangnameEntity {
    private int id;
    private int idkategor;
    private String name;
    private int lang;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdkategor() {
        return idkategor;
    }

    public void setIdkategor(int idkategor) {
        this.idkategor = idkategor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLang() {
        return lang;
    }

    public void setLang(int lang) {
        this.lang = lang;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KategorlangnameEntity that = (KategorlangnameEntity) o;

        if (id != that.id) return false;
        if (idkategor != that.idkategor) return false;
        if (lang != that.lang) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idkategor;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + lang;
        return result;
    }
}
