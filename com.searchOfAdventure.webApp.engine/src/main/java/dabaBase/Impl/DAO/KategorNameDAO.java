package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.KategorlangnameEntity;

/**
 * Created by Admin on 13.03.2016.
 */
public interface KategorNameDAO {
    void addKategorName (KategorlangnameEntity kategorlangnameEntity);
    void updateKategorName (KategorlangnameEntity kategorlangnameEntity);
    void delategorName (KategorlangnameEntity kategorlangnameEntity);

    KategorlangnameEntity getKategorNameById (int id);
}
