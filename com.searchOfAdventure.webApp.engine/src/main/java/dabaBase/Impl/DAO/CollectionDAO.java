package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.CollectionEntity;
import engine.apiV1_0b.meny.optionNew.Collection;

import java.util.List;

/**
 * Created by Admin on 27.02.2016.
 */
public interface CollectionDAO {
    void addCollection (CollectionEntity collection);
    void delCollection (CollectionEntity collection);
    void updateCollection (CollectionEntity collection);
    CollectionEntity getCollectionById (int id);
    List<CollectionEntity> getAllCollection ();
    List<CollectionEntity> getAllCollectionByThisUser () throws Exception;
    CollectionEntity getCollectionByThisUser (int idCollection) throws Exception;
}
