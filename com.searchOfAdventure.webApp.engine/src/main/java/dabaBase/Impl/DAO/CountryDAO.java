package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.CountryEntity;
import dabaBase.Impl.DAO.Entity.CountrylangnameEntity;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public interface CountryDAO {
    void createCountry (CountryEntity countryEntity);
    void destroyCountry (CountryEntity countryEntity);
    void destroyCountry (CountrylangnameEntity countryEntity);
    List<CountryEntity> getAllCountry ();

    public void addCountry (CountrylangnameEntity countryEntity);
    public void delCountry (CountrylangnameEntity countryEntity);
    public void updateCountry (CountrylangnameEntity countryEntity);

    CountrylangnameEntity getCountryById (int idCountry);
    CountrylangnameEntity getCountryByName (String nameCountry);

}
