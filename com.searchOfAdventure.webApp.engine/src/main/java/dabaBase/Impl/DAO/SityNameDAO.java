package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.SitylangnameEntity;

/**
 * Created by Admin on 13.03.2016.
 */
public interface SityNameDAO {
    void addSityName (SitylangnameEntity sitylangnameEntity);
    void updateSityName (SitylangnameEntity sitylangnameEntity);
    void dellSityName (SitylangnameEntity sitylangnameEntity);

    SitylangnameEntity getSityNameById (int idSity);
    SitylangnameEntity getSityNameByName (int idCountry,String nameSity);
}
