package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.IntereslangnameEntity;

/**
 * Created by Admin on 13.03.2016.
 */
public interface InteresNameDAO {
    void addInteresName (IntereslangnameEntity intereslangnameEntity);
    void updateInteresName (IntereslangnameEntity intereslangnameEntity);
    void delInteresName (IntereslangnameEntity intereslangnameEntity);

    IntereslangnameEntity getInteresNameById (int id);
    IntereslangnameEntity getInteresNameByName (String name);
}
