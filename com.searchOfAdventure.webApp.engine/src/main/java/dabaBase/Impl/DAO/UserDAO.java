package dabaBase.Impl.DAO;


import dabaBase.Impl.DAO.Entity.InteresEntity;
import dabaBase.Impl.DAO.Entity.UsersEntity;
import java.util.List;

/**
 * Created by Admin on 27.02.2016.
 */
public interface UserDAO {
    void addUser (UsersEntity user);
    void delUser (UsersEntity user);
    void updateUser (UsersEntity user);
    UsersEntity getUserById (int id);
    UsersEntity getUserByU_id (String u_id);
    UsersEntity getUserByEmailAndPassword (String email, String password);
    List<UsersEntity> getAllUser ();

}
