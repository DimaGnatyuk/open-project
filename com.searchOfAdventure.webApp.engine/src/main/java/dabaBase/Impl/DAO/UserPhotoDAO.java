package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.UserphotoEntity;

import java.util.List;

/**
 * Created by Admin on 12.03.2016.
 */
public interface UserPhotoDAO {
    void addPhoto (UserphotoEntity userphotoEntity);
    void delPhoto (UserphotoEntity userphotoEntity);

    UserphotoEntity getPhotoById (int idPhoto);
    List<UserphotoEntity> getPhotoByUser (int idAlbum);
}
