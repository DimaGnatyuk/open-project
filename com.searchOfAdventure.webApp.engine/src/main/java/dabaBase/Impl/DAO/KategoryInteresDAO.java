package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.KategorinteresEntity;
import dabaBase.Impl.DAO.Entity.KategorlangnameEntity;
import engine.apiV1_0b.meny.optionNew.Collection;

import java.util.List;

/**
 * Created by Admin on 10.03.2016.
 */
public interface KategoryInteresDAO {
    void createKategoryInteres (KategorinteresEntity kategorinteresEntity);//Створення нового інтересу
    void deleteKategoryInteres (int idKategoryInteres);//Видалення по ід
    List<KategorinteresEntity> getAllKategoryInteres ();//виведення списку всих категорій

    void setLengNameKategoryInteres (KategorlangnameEntity nameKategoryInteres);//Встановлення локалізації
    void updateLengNameKategoryInteres (KategorlangnameEntity nameKategoryInteres);//Оновлення локалізації
    void delLengNameKategoryInteres (KategorlangnameEntity nameKategoryInteres);//Видалення локалізації

    KategorlangnameEntity getKategoryInteresById (int KategoryInteresId); //поверення по ід
    KategorlangnameEntity getKategoryInteresByName (String KategoryName); //поверення по назві

    List<KategorlangnameEntity> getAllKategoryInteres (int idLanguage); //поверення пакету по ід мовного пакету
}
