package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.MessageFriendEntity;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public interface MessageFriendsDAO {
    void addMessage (MessageFriendEntity messageFriendEntity);
    void delMessage (MessageFriendEntity messageFriendEntity);
    List<MessageFriendEntity> getAllMessage ();
    MessageFriendEntity getMessageById (int idMessageFriendEntity);
}
