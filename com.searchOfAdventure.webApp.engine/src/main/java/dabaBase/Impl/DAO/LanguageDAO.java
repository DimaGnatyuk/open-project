package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.LanguagesEntity;

import java.util.List;

/**
 * Created by Admin on 13.03.2016.
 */
public interface LanguageDAO {
    void addLanguage (LanguagesEntity languagesEntity);
    void updateLanguage (LanguagesEntity languagesEntity);
    void delLanguage (LanguagesEntity languagesEntity);

    LanguagesEntity getLanguageById (int id);
    LanguagesEntity getLanguageByTeg (String teg);
    LanguagesEntity getLanguageByName (String name);
    List<LanguagesEntity> getAllLanguage ();

    List<LanguagesEntity> getFullAllLanguage ();/*Ignore Enabled*/
}
