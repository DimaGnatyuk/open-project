package dabaBase.Impl.DAO;

import dabaBase.Impl.DAO.Entity.ContestantsEntity;
import dabaBase.Impl.DAO.Entity.InteresuserEntity;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public interface CollectionUserDAO {
    void addCollection (InteresuserEntity interesuserEntity);
    void delCollection (InteresuserEntity interesuserEntity);
    List<ContestantsEntity> getAllCollectionByIdUser (int idUser);
    /*ContestantsEntity getCollectionUser (int idUser, int idCollection);*/
}
