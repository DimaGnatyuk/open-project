package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.CollectionEntity;
import dabaBase.Util.HibernateUtil;
import dabaBase.Service.Factory;
import engine.apiV1_0b.meny.optionNew.Collection;
import dabaBase.Impl.DAO.CollectionDAO;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import servlet.EngineSession;

import java.util.List;

/**
 * Created by Admin on 27.02.2016.
 */
public class CollectionDAOImpl implements CollectionDAO {
    @Override
    public void addCollection(CollectionEntity collection) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(collection);
        session.getTransaction().commit();
    }

    @Override
    public void delCollection(CollectionEntity collection) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(collection);
        session.getTransaction().commit();
    }

    @Override
    public void updateCollection(CollectionEntity collection) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(collection);
        session.getTransaction().commit();
    }

    @Override
    public CollectionEntity getCollectionById(int id) {
        CollectionEntity collection = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        collection = (CollectionEntity) session.load(CollectionEntity.class, id);
        session.getTransaction().commit();
        return collection;
    }

    @Override
    public List<CollectionEntity> getAllCollection() {
        List<CollectionEntity> collectionList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        collectionList = session.createCriteria(CollectionEntity.class).list();
        session.getTransaction().commit();
        return collectionList;
    }

    @Override
    public List<CollectionEntity> getAllCollectionByThisUser() throws Exception {
        List<CollectionEntity> collectionList = null;
        int idUser = EngineSession.getId();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        collectionList = session.createCriteria(CollectionEntity.class).add(Restrictions.eq("idadmin", EngineSession.getId())).list();
        //collectionList = session.createCriteria(CollectionEntity.class).add(Restrictions.eq("idadmin", EngineSession.getId())).list();
        session.getTransaction().commit();
        return collectionList;
    }

    @Override
    public CollectionEntity getCollectionByThisUser(int idCollection) throws Exception {
        CollectionEntity collectionList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        collectionList = (CollectionEntity) session.createCriteria(CollectionEntity.class).add(Restrictions.eq("idadmin", EngineSession.getId())).add(Restrictions.idEq(idCollection)).uniqueResult();
        session.getTransaction().commit();
        return collectionList;
    }
}
