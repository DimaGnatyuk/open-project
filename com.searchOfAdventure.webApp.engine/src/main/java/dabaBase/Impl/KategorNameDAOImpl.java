package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.KategorlangnameEntity;
import dabaBase.Impl.DAO.KategorNameDAO;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created by Admin on 13.03.2016.
 */
public class KategorNameDAOImpl implements KategorNameDAO {
    @Override
    public void addKategorName(KategorlangnameEntity kategorlangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(kategorlangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public void updateKategorName(KategorlangnameEntity kategorlangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(kategorlangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public void delategorName(KategorlangnameEntity kategorlangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(kategorlangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public KategorlangnameEntity getKategorNameById(int id) {
        KategorlangnameEntity kategorlangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        kategorlangnameEntity = (KategorlangnameEntity) session.createCriteria(KategorlangnameEntity.class).add(Restrictions.eq("idkategor",id)).uniqueResult();
        session.getTransaction().commit();
        return kategorlangnameEntity;
    }
}
