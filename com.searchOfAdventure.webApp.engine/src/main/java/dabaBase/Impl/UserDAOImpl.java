package dabaBase.Impl;

import component.SystemComponent;
import dabaBase.Impl.DAO.Entity.InteresEntity;
import dabaBase.Impl.DAO.Entity.UsersEntity;
import dabaBase.Impl.DAO.UserDAO;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import systemFunction.SystemFunction;

import java.util.List;

/**
 * Created by Admin on 27.02.2016.
 */
public class UserDAOImpl implements UserDAO {

    @Override
    public void addUser(UsersEntity user) {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
    }

    @Override
    public void delUser(UsersEntity user) {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
    }

    @Override
    public void updateUser(UsersEntity user) {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
    }

    @Override
    public UsersEntity getUserById(int id) {
        UsersEntity usersEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        usersEntity = (UsersEntity) session.load(UsersEntity.class,id);
        session.getTransaction().commit();
        return usersEntity;
    }

    @Override
    public UsersEntity getUserByU_id(String u_id) {
        UsersEntity usersEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        usersEntity = (UsersEntity) session.createCriteria(UsersEntity.class).add(Restrictions.like("uId",u_id)).uniqueResult();
        session.getTransaction().commit();
        return usersEntity;
    }

    @Override
    public UsersEntity getUserByEmailAndPassword (String email, String password){
        return getUserByU_id(SystemComponent.getMd5(email+password));
    }

    @Override
    public List<UsersEntity> getAllUser() {
        List<UsersEntity> usersEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        usersEntity = session.createCriteria(UsersEntity.class).list();
        session.getTransaction().commit();
        return usersEntity;
    }
}
