package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.KategorinteresEntity;
import dabaBase.Impl.DAO.Entity.KategorlangnameEntity;
import dabaBase.Impl.DAO.Entity.UsersEntity;
import dabaBase.Impl.DAO.KategoryInteresDAO;
import dabaBase.Service.Factory;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;


import java.util.List;

/**
 * Created by Admin on 10.03.2016.
 */
public class KategoryInteresDAOImpl implements KategoryInteresDAO {
    @Override
    public void createKategoryInteres(KategorinteresEntity kategorinteresEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(kategorinteresEntity);
        session.getTransaction().commit();
    }

    @Override
    public void deleteKategoryInteres(int idKategoryInteres) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        KategorinteresEntity kategorinteresEntity = new KategorinteresEntity();
        kategorinteresEntity.setId(idKategoryInteres);
        session.delete(kategorinteresEntity);
        session.getTransaction().commit();
    }

    @Override
    public List<KategorinteresEntity> getAllKategoryInteres() {
        List<KategorinteresEntity> kategorinteresEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        kategorinteresEntityList = session.createCriteria(KategorinteresEntity.class).list();
        session.getTransaction().commit();
        return kategorinteresEntityList;
    }

    @Override
    public void setLengNameKategoryInteres(KategorlangnameEntity nameKategoryInteres) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(nameKategoryInteres);
        session.getTransaction().commit();
    }

    @Override
    public void updateLengNameKategoryInteres(KategorlangnameEntity nameKategoryInteres) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(nameKategoryInteres);
        session.getTransaction().commit();
    }

    @Override
    public void delLengNameKategoryInteres(KategorlangnameEntity nameKategoryInteres) {
        int idKategor = nameKategoryInteres.getIdkategor();
        int count = 0;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(nameKategoryInteres);
        count = session.createCriteria(nameKategoryInteres.getClass()).add(Restrictions.eq("idkategor",idKategor)).list().size();
        if (count == 0){
            KategorinteresEntity kategorinteresEntity = new KategorinteresEntity();
            kategorinteresEntity.setId(idKategor);
            session.delete(kategorinteresEntity);
        }
        session.getTransaction().commit();
    }

    @Override
    public KategorlangnameEntity getKategoryInteresById(int KategoryInteresId) {
        KategorlangnameEntity kategorinteresEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        kategorinteresEntity = (KategorlangnameEntity)session.load(KategorinteresEntity.class,KategoryInteresId);
        session.getTransaction().commit();
        return kategorinteresEntity;
    }

    @Override
    public KategorlangnameEntity getKategoryInteresByName(String KategoryName) {
        KategorlangnameEntity kategorlangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        kategorlangnameEntity = (KategorlangnameEntity)session.createCriteria(KategorlangnameEntity.class)
                .add(Restrictions.like("name",KategoryName)).uniqueResult();
        session.getTransaction().commit();
        return kategorlangnameEntity;
    }

    @Override
    public List<KategorlangnameEntity> getAllKategoryInteres(int idLanguage) {
        List<KategorlangnameEntity> kategorinteresEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        kategorinteresEntity = session.createCriteria(KategorlangnameEntity.class)
                .add(Restrictions.eq("lang",idLanguage))
                .list();
        session.getTransaction().commit();
        return kategorinteresEntity;
    }
}
