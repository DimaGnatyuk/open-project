package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.UseralbumEntity;
import dabaBase.Impl.DAO.Entity.UsersEntity;
import dabaBase.Impl.DAO.UserAlbumDAO;
import dabaBase.Service.Factory;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public class UserAlbumDAOImpl implements UserAlbumDAO {
    @Override
    public void addAlbum(UseralbumEntity useralbumEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(useralbumEntity);
        session.getTransaction().commit();
    }

    @Override
    public void updateAlbum(UseralbumEntity useralbumEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(useralbumEntity);
        session.getTransaction().commit();
    }

    @Override
    public void deleteAlbum(UseralbumEntity useralbumEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(useralbumEntity);
        session.getTransaction().commit();
    }

    @Override
    public UseralbumEntity getAlbumById(int idAlbum) {
        UseralbumEntity useralbumEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        useralbumEntity = (UseralbumEntity)session.load(useralbumEntity.getClass(),idAlbum);
        session.getTransaction().commit();
        return useralbumEntity;
    }

    @Override
    public List<UseralbumEntity> getAllAlbum(int idUser) {
        List<UseralbumEntity> useralbumEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        useralbumEntityList = session.createCriteria(UseralbumEntity.class).add(Restrictions.eq("iduser",idUser)).list();
        session.getTransaction().commit();
        return useralbumEntityList;
    }

    @Override
    public List<UseralbumEntity> getAllAlbum(String idUser) {
        UsersEntity usersEntity = Factory.getInstance().getUserDAO().getUserByU_id(idUser);
        List<UseralbumEntity> useralbumEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        useralbumEntityList = session.createCriteria(UseralbumEntity.class).add(Restrictions.eq("iduser",usersEntity.getId())).list();
        session.getTransaction().commit();
        return useralbumEntityList;
    }

}
