package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.InteresEntity;
import dabaBase.Impl.DAO.Entity.IntereslangnameEntity;
import dabaBase.Impl.DAO.Entity.KategorlangnameEntity;
import dabaBase.Impl.DAO.Entity.UsersEntity;
import dabaBase.Impl.DAO.InteresDAO;
import dabaBase.Util.HibernateUtil;
import engine.apiV1_0b.meny.optionNew.Collection;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Admin on 10.03.2016.
 */
public class InteresDAOImpl implements InteresDAO {

    @Override
    public void createInteres(InteresEntity interes) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(interes);
        session.getTransaction().commit();
    }

    @Override
    public void destroyInteres(InteresEntity interes) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(interes);
        session.getTransaction().commit();
    }

    @Override
    public void getAllIdInteres(int idKategori) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
    }

    @Override
    public void getAllIdInteres(KategorlangnameEntity Kategori) {

    }

    @Override
    public void addInteres(IntereslangnameEntity interes) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(interes);
        session.getTransaction().commit();
    }

    @Override
    public void delInteres(IntereslangnameEntity interes) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(interes);
        session.getTransaction().commit();
    }

    @Override
    public void updateInteres(IntereslangnameEntity interes) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(interes);
        session.getTransaction().commit();
    }

    @Override
    public IntereslangnameEntity getInteresById(int id) {
        IntereslangnameEntity interesEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        interesEntity = (IntereslangnameEntity)session.createCriteria(IntereslangnameEntity.class).add(Restrictions.eq("idintere",id)).uniqueResult();
        session.getTransaction().commit();
        return interesEntity;
    }

    @Override
    public IntereslangnameEntity getInteresByName(String name) {
        IntereslangnameEntity interesEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        interesEntity = (IntereslangnameEntity)session.createCriteria(interesEntity.getClass()).add(Restrictions.eq("name",name)).uniqueResult();
        session.getTransaction().commit();
        return interesEntity;
    }

    @Override
    public List<InteresEntity> getAllInteresEntityByIdKateguri(int idKategori) {
        List<InteresEntity> interesEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        interesEntityList = session.createCriteria(InteresEntity.class).add(Restrictions.eq("idkategor",idKategori)).list();
        session.getTransaction().commit();
        return interesEntityList;
    }

    @Override
    public List<InteresEntity> getAllInteresEntityByKateguri(KategorlangnameEntity Kategori) {
        return getAllInteresEntityByIdKateguri(Kategori.getIdkategor());
    }
}
