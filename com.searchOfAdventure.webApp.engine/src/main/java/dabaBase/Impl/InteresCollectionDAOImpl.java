package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.InteresEntity;
import dabaBase.Impl.DAO.Entity.InterescollectionEntity;
import dabaBase.Impl.DAO.Entity.IntereslangnameEntity;
import dabaBase.Impl.DAO.InteresCollectionDAO;
import dabaBase.Service.Factory;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Admin on 12.03.2016.
 */
public class InteresCollectionDAOImpl implements InteresCollectionDAO {
    @Override
    public void addInteres(InterescollectionEntity interescollectionEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(interescollectionEntity);
        session.getTransaction().commit();
    }

    @Override
    public void delInteres(InterescollectionEntity interescollectionEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(interescollectionEntity);
        session.getTransaction().commit();
    }

    @Override
    public List<InterescollectionEntity> getInteresByCollectionId(int idCollection) {
        List<InterescollectionEntity> interescollectionEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        interescollectionEntityList = session.createCriteria(InterescollectionEntity.class).add(Restrictions.eq("idcollection",idCollection)).list();
        session.getTransaction().commit();
        return interescollectionEntityList;
    }

    @Override
    public InterescollectionEntity getInteresByCollectionIdAndInteres(int idCollection, String nameInteres) {
        IntereslangnameEntity intereslangnameEntity = Factory.getInstance().getInteresNameDAO().getInteresNameByName(nameInteres);
        InterescollectionEntity interescollectionEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        interescollectionEntity = (InterescollectionEntity) session.createCriteria(InterescollectionEntity.class).add(Restrictions.eq("idcollection",idCollection)).add(Restrictions.eq("idinteres",intereslangnameEntity.getId())).uniqueResult();
        session.getTransaction().commit();
        return interescollectionEntity;
    }
}
