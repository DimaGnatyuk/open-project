package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.IntereslangnameEntity;
import dabaBase.Impl.DAO.InteresNameDAO;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created by Admin on 13.03.2016.
 */
public class InteresNameDAOImpl implements InteresNameDAO {
    @Override
    public void addInteresName(IntereslangnameEntity intereslangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(intereslangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public void updateInteresName(IntereslangnameEntity intereslangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(intereslangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public void delInteresName(IntereslangnameEntity intereslangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(intereslangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public IntereslangnameEntity getInteresNameById(int id) {
        IntereslangnameEntity intereslangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        intereslangnameEntity = (IntereslangnameEntity) session.createCriteria(IntereslangnameEntity.class).add(Restrictions.eq("idintere", id)).uniqueResult();
        session.getTransaction().commit();
        return intereslangnameEntity;
    }

    @Override
    public IntereslangnameEntity getInteresNameByName(String name) {
        IntereslangnameEntity intereslangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        intereslangnameEntity = (IntereslangnameEntity) session.createCriteria(IntereslangnameEntity.class).add(Restrictions.like("name", name)).uniqueResult();
        session.getTransaction().commit();
        return intereslangnameEntity;
    }


}
