package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.IntereslangnameEntity;
import dabaBase.Impl.DAO.Entity.InteresuserEntity;
import dabaBase.Impl.DAO.InteresUserDAO;
import dabaBase.Service.Factory;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import servlet.EngineSession;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public class InteresUserDAOImpl implements InteresUserDAO {

    @Override
    public void addInteres(InteresuserEntity interesuserEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(interesuserEntity);
        session.getTransaction().commit();
    }

    @Override
    public void delInteres(InteresuserEntity interesuserEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(interesuserEntity);
        session.getTransaction().commit();
    }

    @Override
    public List<InteresuserEntity> getAllInteresByIdUser(int idUser) {
        List<InteresuserEntity> interesuserEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        interesuserEntityList = session.createCriteria(InteresuserEntity.class).add(Restrictions.eq("iduser",idUser)).list();
        session.getTransaction().commit();
        return interesuserEntityList;
    }

    @Override
    public InteresuserEntity getMyInteresByIdInteres(String interes) throws Exception {
        IntereslangnameEntity interesEntity = Factory.getInstance().getInteresNameDAO().getInteresNameByName(interes);
        int myId = EngineSession.getId();

        InteresuserEntity interesuserEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        interesuserEntity = (InteresuserEntity) session.createCriteria(InteresuserEntity.class).add(Restrictions.eq("iduser",myId)).add(Restrictions.eq("idinteres",interesEntity.getId())).uniqueResult();
        session.getTransaction().commit();
        return interesuserEntity;
    }
}
