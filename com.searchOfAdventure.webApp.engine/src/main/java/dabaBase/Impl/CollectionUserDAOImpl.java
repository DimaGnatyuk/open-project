package dabaBase.Impl;

import dabaBase.Impl.DAO.CollectionUserDAO;
import dabaBase.Impl.DAO.Entity.ContestantsEntity;
import dabaBase.Impl.DAO.Entity.InteresuserEntity;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public class CollectionUserDAOImpl implements CollectionUserDAO {
    @Override
    public void addCollection(InteresuserEntity interesuserEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(interesuserEntity);
        session.getTransaction().commit();
    }

    @Override
    public void delCollection(InteresuserEntity interesuserEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(interesuserEntity);
        session.getTransaction().commit();
    }

    @Override
    public List<ContestantsEntity> getAllCollectionByIdUser(int idUser) {
        List<ContestantsEntity> interesuserEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        interesuserEntity = session.createCriteria(ContestantsEntity.class).add(Restrictions.eq("iduser",idUser)).list();
        session.getTransaction().commit();
        return interesuserEntity;
    }

   /* @Override
    public ContestantsEntity getCollectionUser(int idUser, int idCollection) {
       ContestantsEntity interesuserEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        interesuserEntity = (ContestantsEntity) session.createCriteria(ContestantsEntity.class).add(Restrictions.eq("iduser",idUser)).add(Restrictions.eq("idcollection",idCollection)).uniqueResult();
        session.getTransaction().commit();
        return interesuserEntity;
    }*/
}
