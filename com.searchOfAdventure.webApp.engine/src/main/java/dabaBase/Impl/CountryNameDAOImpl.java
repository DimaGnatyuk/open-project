package dabaBase.Impl;

import dabaBase.Impl.DAO.CountryNameDAO;
import dabaBase.Impl.DAO.Entity.CountryEntity;
import dabaBase.Impl.DAO.Entity.CountrylangnameEntity;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created by Admin on 13.03.2016.
 */
public class CountryNameDAOImpl implements CountryNameDAO {
    @Override
    public void addCountryName(CountrylangnameEntity countrylangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(countrylangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public void updateCountryName(CountrylangnameEntity countrylangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(countrylangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public void dellCountryName(CountrylangnameEntity countrylangnameEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(countrylangnameEntity);
        session.getTransaction().commit();
    }

    @Override
    public CountrylangnameEntity getCountryNameById(int idCountry) {
        CountrylangnameEntity countrylangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        countrylangnameEntity = (CountrylangnameEntity) session.load(CountrylangnameEntity.class,idCountry);
        session.getTransaction().commit();
        return countrylangnameEntity;
    }

    public CountrylangnameEntity getCountryById(int idCountry) {
        CountrylangnameEntity countrylangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        countrylangnameEntity = (CountrylangnameEntity) session.createCriteria(CountrylangnameEntity.class).add(Restrictions.eq("idcountry",idCountry)).uniqueResult();
        session.getTransaction().commit();
        return countrylangnameEntity;
    }

    @Override
    public CountrylangnameEntity getCountryNameByName(String nameCountry) {
        CountrylangnameEntity countrylangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        countrylangnameEntity = (CountrylangnameEntity) session.createCriteria(CountrylangnameEntity.class).add(Restrictions.eq("name","Україні")).uniqueResult();
        session.getTransaction().commit();
        return countrylangnameEntity;
    }
}
