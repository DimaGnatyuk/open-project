package dabaBase.Impl;

import dabaBase.Impl.DAO.Entity.SityEntity;
import dabaBase.Impl.DAO.SityDAO;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Admin on 13.03.2016.
 */
public class SityDAOImpl implements SityDAO {
    @Override
    public void addSity(SityEntity sityEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(sityEntity);
        session.getTransaction().commit();
    }

    @Override
    public void updateSity(SityEntity sityEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(sityEntity);
        session.getTransaction().commit();
    }

    @Override
    public void dellSity(SityEntity sityEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(sityEntity);
        session.getTransaction().commit();
    }

    @Override
    public SityEntity getSityById(int id) {
        SityEntity sityEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        sityEntity = (SityEntity) session.load(SityEntity.class,id);
        session.getTransaction().commit();
        return sityEntity;
    }

    @Override
    public List<SityEntity> getAllSity(int idCountry) {
        List<SityEntity> sityEntityList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        sityEntityList = session.createCriteria(SityEntity.class).add(Restrictions.eq("idcountry",idCountry)).list();
        session.getTransaction().commit();
        return sityEntityList;
    }
}
