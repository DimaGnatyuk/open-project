package dabaBase.Impl;

import dabaBase.Impl.DAO.CountryDAO;
import dabaBase.Impl.DAO.Entity.CountryEntity;
import dabaBase.Impl.DAO.Entity.CountrylangnameEntity;
import dabaBase.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Admin on 11.03.2016.
 */
public class CountryDAOImpl implements CountryDAO {
    @Override
    public void createCountry(CountryEntity countryEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(countryEntity);
        session.getTransaction().commit();
    }

    @Override
    public void destroyCountry(CountryEntity countryEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(countryEntity);
        session.getTransaction().commit();
    }

    @Override
    public void destroyCountry(CountrylangnameEntity countryEntity) {

    }

    @Override
    public List<CountryEntity> getAllCountry() {
        List<CountryEntity> countryEntities = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        countryEntities = session.createCriteria(CountryEntity.class).list();
        session.getTransaction().commit();
        return countryEntities;
    }

    @Override
    public void addCountry(CountrylangnameEntity countryEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(countryEntity);
        session.getTransaction().commit();
    }

    @Override
    public void delCountry(CountrylangnameEntity countryEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(countryEntity);
        session.getTransaction().commit();
    }

    @Override
    public void updateCountry(CountrylangnameEntity countryEntity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(countryEntity);
        session.getTransaction().commit();
    }

    @Override
    public CountrylangnameEntity getCountryById(int idCountry) {
        CountrylangnameEntity countrylangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        countrylangnameEntity = (CountrylangnameEntity) session.createCriteria(CountrylangnameEntity.class).add(Restrictions.eq("idcountry",idCountry)).uniqueResult();
        session.getTransaction().commit();
        return countrylangnameEntity;
    }

    @Override
    public CountrylangnameEntity getCountryByName(String nameCountry) {
        CountrylangnameEntity countrylangnameEntity = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        countrylangnameEntity = (CountrylangnameEntity) session.createCriteria(CountrylangnameEntity.class).add(Restrictions.like("name",nameCountry)).uniqueResult();
        session.getTransaction().commit();
        return countrylangnameEntity;
    }
}
