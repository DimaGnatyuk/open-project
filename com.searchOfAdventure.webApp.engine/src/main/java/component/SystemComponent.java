package component;

import org.apache.commons.codec.digest.DigestUtils;

import java.sql.Timestamp;

/**
 * Created by Admin on 15.03.2016.
 */
public class SystemComponent {
    public static String getMd5 (String s)
    {
        return DigestUtils.md5Hex(s);
    }
    public static Timestamp getTimestamp (){
        long time = System.currentTimeMillis();
        return new Timestamp(time);
    }
}
