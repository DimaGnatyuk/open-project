-- --------------------------------------------------------
-- Сервер:                       127.0.0.1
-- Версія сервера:               5.5.25 - MySQL Community Server (GPL)
-- ОС сервера:                   Win32
-- HeidiSQL Версія:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for my_project
CREATE DATABASE IF NOT EXISTS `my_project` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `my_project`;


-- Dumping structure for таблиця my_project.collection
CREATE TABLE IF NOT EXISTS `collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `word` text,
  `logo` text,
  `country` int(11) NOT NULL,
  `sity` int(11) NOT NULL,
  `adress` text NOT NULL,
  `dateof` date DEFAULT NULL,
  `idadmin` int(11) NOT NULL,
  `title` text,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_collection_users` (`idadmin`),
  KEY `FK_collection_country` (`country`),
  KEY `FK_collection_sity` (`sity`),
  CONSTRAINT `FK_collection_country` FOREIGN KEY (`country`) REFERENCES `country` (`id`),
  CONSTRAINT `FK_collection_sity` FOREIGN KEY (`sity`) REFERENCES `sity` (`id`),
  CONSTRAINT `FK_collection_users` FOREIGN KEY (`idadmin`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.collection: ~0 rows (приблизно)
DELETE FROM `collection`;
/*!40000 ALTER TABLE `collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `collection` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.contestants
CREATE TABLE IF NOT EXISTS `contestants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcollection` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idcollection_iduser` (`idcollection`,`iduser`),
  KEY `FK_contestants_users` (`iduser`),
  CONSTRAINT `FK_contestants_collection` FOREIGN KEY (`idcollection`) REFERENCES `collection` (`id`),
  CONSTRAINT `FK_contestants_users` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.contestants: ~0 rows (приблизно)
DELETE FROM `contestants`;
/*!40000 ALTER TABLE `contestants` DISABLE KEYS */;
/*!40000 ALTER TABLE `contestants` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.country
CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.country: ~1 rows (приблизно)
DELETE FROM `country`;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` (`id`) VALUES
	(1);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.countrylangname
CREATE TABLE IF NOT EXISTS `countrylangname` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcountry` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lang` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `idcountry_lang` (`idcountry`,`lang`) USING HASH,
  KEY `FK_countrylangname_languages` (`lang`),
  CONSTRAINT `FK_countrylangname_country` FOREIGN KEY (`idcountry`) REFERENCES `country` (`id`),
  CONSTRAINT `FK_countrylangname_languages` FOREIGN KEY (`lang`) REFERENCES `languages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.countrylangname: ~1 rows (приблизно)
DELETE FROM `countrylangname`;
/*!40000 ALTER TABLE `countrylangname` DISABLE KEYS */;
INSERT INTO `countrylangname` (`id`, `idcountry`, `name`, `lang`) VALUES
	(4, 1, 'Україні', 1);
/*!40000 ALTER TABLE `countrylangname` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.friends
CREATE TABLE IF NOT EXISTS `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mdsum` varchar(50) NOT NULL DEFAULT '0',
  `id_user_reqeust` int(11) NOT NULL,
  `id_user_responce` int(11) NOT NULL,
  `validation` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_user_reqeust_id_user_responce` (`id_user_reqeust`,`id_user_responce`),
  UNIQUE KEY `mdsum` (`mdsum`),
  KEY `FK_friends_users_2` (`id_user_responce`),
  CONSTRAINT `FK_friends_users` FOREIGN KEY (`id_user_reqeust`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_friends_users_2` FOREIGN KEY (`id_user_responce`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.friends: ~0 rows (приблизно)
DELETE FROM `friends`;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.interes
CREATE TABLE IF NOT EXISTS `interes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkategor` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_interes_kategorinteres` (`idkategor`),
  CONSTRAINT `FK_interes_kategorinteres` FOREIGN KEY (`idkategor`) REFERENCES `kategorinteres` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.interes: ~3 rows (приблизно)
DELETE FROM `interes`;
/*!40000 ALTER TABLE `interes` DISABLE KEYS */;
INSERT INTO `interes` (`id`, `idkategor`) VALUES
	(1, 1),
	(2, 1),
	(3, 2);
/*!40000 ALTER TABLE `interes` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.interescollection
CREATE TABLE IF NOT EXISTS `interescollection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcollection` int(11) NOT NULL,
  `idinteres` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idcollection_idinteres` (`idcollection`,`idinteres`) USING HASH,
  KEY `FK_interescollection_interes` (`idinteres`),
  CONSTRAINT `FK_interescollection_collection` FOREIGN KEY (`idcollection`) REFERENCES `collection` (`id`),
  CONSTRAINT `FK_interescollection_interes` FOREIGN KEY (`idinteres`) REFERENCES `interes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.interescollection: ~0 rows (приблизно)
DELETE FROM `interescollection`;
/*!40000 ALTER TABLE `interescollection` DISABLE KEYS */;
/*!40000 ALTER TABLE `interescollection` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.intereslangname
CREATE TABLE IF NOT EXISTS `intereslangname` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idintere` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lang` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `Індекс 5` (`idintere`,`lang`) USING HASH,
  KEY `FK_intereslangname_languages` (`lang`),
  KEY `idintere_name_lang` (`idintere`),
  CONSTRAINT `FK_intereslangname_interes` FOREIGN KEY (`idintere`) REFERENCES `interes` (`id`),
  CONSTRAINT `FK_intereslangname_languages` FOREIGN KEY (`lang`) REFERENCES `languages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.intereslangname: ~2 rows (приблизно)
DELETE FROM `intereslangname`;
/*!40000 ALTER TABLE `intereslangname` DISABLE KEYS */;
INSERT INTO `intereslangname` (`id`, `idintere`, `name`, `lang`) VALUES
	(1, 1, 'Покатушки', 1),
	(15, 2, 'Прогулянк', 1);
/*!40000 ALTER TABLE `intereslangname` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.interesuser
CREATE TABLE IF NOT EXISTS `interesuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idinteres` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iduser_idinteres` (`iduser`,`idinteres`),
  KEY `FK_interesuser_interes` (`idinteres`),
  CONSTRAINT `FK_interesuser_interes` FOREIGN KEY (`idinteres`) REFERENCES `interes` (`id`),
  CONSTRAINT `FK_interesuser_users` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.interesuser: ~0 rows (приблизно)
DELETE FROM `interesuser`;
/*!40000 ALTER TABLE `interesuser` DISABLE KEYS */;
/*!40000 ALTER TABLE `interesuser` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.kategorinteres
CREATE TABLE IF NOT EXISTS `kategorinteres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.kategorinteres: ~2 rows (приблизно)
DELETE FROM `kategorinteres`;
/*!40000 ALTER TABLE `kategorinteres` DISABLE KEYS */;
INSERT INTO `kategorinteres` (`id`) VALUES
	(1),
	(2);
/*!40000 ALTER TABLE `kategorinteres` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.kategorlangname
CREATE TABLE IF NOT EXISTS `kategorlangname` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkategor` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lang` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `idkategor_lang` (`idkategor`,`lang`) USING HASH,
  KEY `FK_kategorlangname_languages` (`lang`),
  CONSTRAINT `FK_kategorlangname_kategorinteres` FOREIGN KEY (`idkategor`) REFERENCES `kategorinteres` (`id`),
  CONSTRAINT `FK_kategorlangname_languages` FOREIGN KEY (`lang`) REFERENCES `languages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.kategorlangname: ~1 rows (приблизно)
DELETE FROM `kategorlangname`;
/*!40000 ALTER TABLE `kategorlangname` DISABLE KEYS */;
INSERT INTO `kategorlangname` (`id`, `idkategor`, `name`, `lang`) VALUES
	(2, 1, 'Спорт', 1);
/*!40000 ALTER TABLE `kategorlangname` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.languages
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `teg` varchar(2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `teg` (`teg`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.languages: ~1 rows (приблизно)
DELETE FROM `languages`;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` (`id`, `name`, `teg`, `enabled`) VALUES
	(1, 'Українська', 'ua', 1);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.message_friend
CREATE TABLE IF NOT EXISTS `message_friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `summmd` text,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `message` text NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_message_friend_users` (`sender`),
  KEY `FK_message_friend_users_2` (`receiver`),
  CONSTRAINT `FK_message_friend_users` FOREIGN KEY (`sender`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_message_friend_users_2` FOREIGN KEY (`receiver`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.message_friend: ~0 rows (приблизно)
DELETE FROM `message_friend`;
/*!40000 ALTER TABLE `message_friend` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_friend` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.sity
CREATE TABLE IF NOT EXISTS `sity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcountry` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sity_country` (`idcountry`),
  CONSTRAINT `FK_sity_country` FOREIGN KEY (`idcountry`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.sity: ~1 rows (приблизно)
DELETE FROM `sity`;
/*!40000 ALTER TABLE `sity` DISABLE KEYS */;
INSERT INTO `sity` (`id`, `idcountry`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `sity` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.sitylangname
CREATE TABLE IF NOT EXISTS `sitylangname` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsity` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lang` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idsity_lang` (`idsity`,`lang`) USING HASH,
  UNIQUE KEY `name` (`name`) USING HASH,
  KEY `FK_sitylangname_languages` (`lang`),
  CONSTRAINT `FK_sitylangname_languages` FOREIGN KEY (`lang`) REFERENCES `languages` (`id`),
  CONSTRAINT `FK_sitylangname_sity` FOREIGN KEY (`idsity`) REFERENCES `sity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.sitylangname: ~1 rows (приблизно)
DELETE FROM `sitylangname`;
/*!40000 ALTER TABLE `sitylangname` DISABLE KEYS */;
INSERT INTO `sitylangname` (`id`, `idsity`, `name`, `lang`) VALUES
	(1, 1, 'Черкаси', 1);
/*!40000 ALTER TABLE `sitylangname` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.useralbum
CREATE TABLE IF NOT EXISTS `useralbum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `name` text NOT NULL,
  `title` text,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_useralbum_users` (`iduser`),
  CONSTRAINT `FK_useralbum_users` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.useralbum: ~0 rows (приблизно)
DELETE FROM `useralbum`;
/*!40000 ALTER TABLE `useralbum` DISABLE KEYS */;
/*!40000 ALTER TABLE `useralbum` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.userphoto
CREATE TABLE IF NOT EXISTS `userphoto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idalbum` int(11) NOT NULL,
  `file_name` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_userphoto_useralbum` (`idalbum`),
  CONSTRAINT `FK_userphoto_useralbum` FOREIGN KEY (`idalbum`) REFERENCES `useralbum` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.userphoto: ~0 rows (приблизно)
DELETE FROM `userphoto`;
/*!40000 ALTER TABLE `userphoto` DISABLE KEYS */;
/*!40000 ALTER TABLE `userphoto` ENABLE KEYS */;


-- Dumping structure for таблиця my_project.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` varchar(50) NOT NULL,
  `name` text,
  `status_user` text,
  `email` varchar(30) NOT NULL,
  `country` int(11) DEFAULT NULL,
  `sity` int(11) DEFAULT NULL,
  `language_UI` text,
  `data_reg` datetime NOT NULL,
  `data_activ` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`u_id`,`email`),
  UNIQUE KEY `u_id` (`u_id`),
  UNIQUE KEY `email` (`email`),
  KEY `FK_users_country` (`country`),
  KEY `FK_users_sity` (`sity`),
  CONSTRAINT `FK_users_country` FOREIGN KEY (`country`) REFERENCES `country` (`id`),
  CONSTRAINT `FK_users_sity` FOREIGN KEY (`sity`) REFERENCES `sity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Dumping data for table my_project.users: ~2 rows (приблизно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `u_id`, `name`, `status_user`, `email`, `country`, `sity`, `language_UI`, `data_reg`, `data_activ`) VALUES
	(19, '74317dcfed2974b7c2f045e39f17ac55', 'Dima Gnatyuk', 'some string', 'dim_mon199717@ukr.net', 1, 1, 'ua', '2016-02-28 15:07:23', NULL),
	(25, 'dfa6e4e5f8f88dcf737bf66160ba67d4', 'Dima Gnatyuk', 'some string', 'dim_mon@ukr.net', 1, 1, 'ua', '2016-02-28 15:20:19', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
